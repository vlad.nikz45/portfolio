<?php

use function PHPSTORM_META\elementType;

function check_pattern($pattern, $stroka)
{
    if (preg_match($pattern, $stroka)) {
        echo 'Да';
    } else {
        echo 'Нет';
    }
}


$task1 = '/^-?\d+$/';
$stroka1 = '12';
// echo check_pattern($task1,$stroka1);

$task2 = '/^[a-zA-Z0-9]+$/';
$stroka2 = '3516ags';
// echo check_pattern($task2,$stroka2);


$task3 = '/^[a-zA-Zа-яА-Я0-9]+$/u';
$stroka3 = 'ячсавфау351asd';
// echo check_pattern($task3,$stroka3);

$task4 = "/^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/";
$stroka4 = 'google.com';
// echo check_pattern($task4,$stroka4);

$task5 = '/^[a-zA-Z][a-zA-Z0-9]{2,24}$/';
$stroka5 = 'Vlad';
// echo check_pattern($task5,$stroka5);

$task6 = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/';
$stroka6 = 'qwertyQ2';

// echo check_pattern($task6,$stroka6);

$task7 = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$&_*^#!])(?!.*\s).{8,}$/';
$stroka7 = 'qw3W!24242';
// echo check_pattern($task7,$stroka7);

$task8 = '/^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$/';
$stroka8 = '2023-04-02';
// echo check_pattern($task8,$stroka8);

$task9 = '#^(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}$#';
$stroka9 = '02/04/2023';
// echo check_pattern($task9,$stroka9);

$task10 = '/^(0[1-9]|1[0-9]|2[0-9]|3[01])[.](0[1-9]|1[012])[.][0-9]{4}$/';
$stroka10 = '02.04.2023';
// echo check_pattern($task10,$stroka10);

$task11 = '/^([0-1]\d|2[0-3])(:[0-5]\d){2}$/';
$stroka11 = '23:44:55';
// echo check_pattern($task11,$stroka11);

$task12 = '/^([0-1]\d|2[0-3])(:[0-5]\d)$/';
$stroka12 = '23:44';
// echo check_pattern($task12,$stroka12);

$task13 = '/^https?:\/\/[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z]{2,}(\/.*)?$/';
$stroka13 = 'http://yandex.ru/refe/2';
// echo check_pattern($task12,$stroka12);

$task14 = '/^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,}$/';
$stroka14 = 'user.123@example.com';
// echo check_pattern($task14,$stroka14);

$task15 = '/((25[0-5]|2[0-4]\d|[01]?\d\d?)\.){3}(25[0-5]|2[0-4]\d|[01]?\d\d?)/';
$stroka15 = '10.1.1.1';
// echo check_pattern($task15,$stroka15);

$task16 = '/^(([0-9a-fA-F]{0,4}):){1,7}([0-9a-fA-F]{0,4})$/';
$stroka16 = '2001:0:9d38:6abd:c70:2d3c:a176:3398';
// echo check_pattern($task16,$stroka16);

$task17 = '/([0-9a-fA-F]{2}([:-]|$)){6}$|([0-9a-fA-F]{3}([.]|$)){4}/';
$stroka17 = 'ec:23:3d:1b:7a:e7';
// echo check_pattern($task17,$stroka17);

$task18 = '/^(\+7|8)\d{10}$/';
$stroka18 = '+79642268061';
// echo check_pattern($task18,$stroka18);

$task19 = '/([0-9]{4}\s?){4}/';
$stroka19 = '4048 4323 9889 3301';
// echo check_pattern($task19,$stroka19);

$task20 = '/^\d{10}|\d{12}$/';
$stroka20 = '3808753981';
// echo check_pattern($task20,$stroka20);

$task21 = '/^\d{6}$/';
$stroka21 = '664000';
// echo check_pattern($task21,$stroka21);

$task22 = '/^([1-9]\d*|\d)(,\d*)?\sруб\.$/';
$stroka22 = '125,21 руб.';
// echo check_pattern($task22,$stroka22);

$task23 = '/^\$([1-9]\d*|\d)(\.\d+)?$/';
$stroka23 = '$0.99';
// echo check_pattern($task23,$stroka23);


?>

<?php

function task2_1($file)
{
    if (preg_match("/^[A-Za-z0-9А-Яа-яёЁ]+\.[a-zA-Z0-9]+$/", $file)) {
        preg_match('/(?<=\.)\w+$/', $file, $matches);
        return $matches[0];
    } else {
        echo "Неверно введено имя файла";
        return false;
    }
}
// echo task2_1('picture.mp3');


function task2_2($file)
{
    $extension = task2_1($file);
    if ($extension != false) {
        if (preg_match("/^(zip|rar|7z|arc|arj|jar|tar)$/", $extension)) {
            return "Архив";
        } else if (preg_match("/^(aac|dts|flac|mp2|mp3|ogg)$/", $extension)) {
            return "Аудиофайл";
        } else if (preg_match("/^(avi|flv|mkv|mp4|asf)$/", $extension)) {
            return "Видеофайл";
        } else if (preg_match("/^(psd|tiff|bmp|jpeg|gif|eps|png)$/", $extension)) {
            return "Картинка";
        } else
            return "Ошибка, формата не существует/не опознан";
    }
}
// echo task2_2('file.avi')

function task2_3($string)
{
    if (preg_match("/<title>(.*?)<\/title>/", $string, $matches)) {
        return $matches[1];
    }
    return "Тег title не содержится в данном коде";
}

// echo task2_3('<html><head><title>Заголовок странички</title></head><body><p>Содержимое страницы</p></body></html>');


function task2_4($string)
{
    if (preg_match_all('/<a\s+[^>]*href=["\']?([^"\'>]+)["\']?[^>]*>/i', $string, $matches)) {


        foreach ($matches[1] as $url) {
            echo $url . "<br>";
        }
    } else {
        return 'Ссылок не найдено';
    }
}
// echo task2_4('<html><body><a href="https://www.php.net/manual/ru/function.preg-match.php">Example</a><a href="http://google.com">Google</a></body></html>')

function task2_5($html)
{
    if (preg_match_all('/<img.*?src=["\'](.*?)["\']/', $html, $matches)) {
        foreach ($matches[1] as $i) {
            echo $i . "<br>";
        }
    } else {
        return 'Ссылок не найдено';
    }
}
// echo task2_5('<html><head><title>Example Page</title></head><body><img src="image1.jpg" alt="Image 1"><img src="image2.png" alt="Image 2"><img src="http://example.com/image3.jpg" alt="Image 3"></body></html>');

function task2_6($text, $search)
{
    $pattern = "/" . preg_quote($search) . "/";
    $replacement = "<strong>$search</strong>";
    $highlighted_text = preg_replace($pattern, $replacement, $text);
    echo $highlighted_text;
}
// task2_6("Это текст, в котором нужно найти и подсветить строку 'нужно найти'.","нужно найти");


function task2_7($text)
{
    $patterns = array(
        '/:\)/',
        '/;\)/',
        '/:\(/',
    );
    $replacements = array(
        '<img src="smile.png" alt=":)">',
        '<img src="wink.png" alt=";)">',
        '<img src="sad.png" alt=":(">',
    );
    $result = preg_replace($patterns, $replacements, $text);
    echo $result;
}

// task2_7('Привет :) мир :(, сейчас подмигну ;)');

function task2_8($text)
{
    $text = preg_replace('/\s+/', ' ', $text);
    echo $text; //
}
task2_8("Эта    строка   содержит   лишние     пробелы?   Да ")

?>