<?php
echo $_POST['delete'];
print_r($_POST['files']);
if (isset($_POST['delete']) && !empty($_POST['files'])) {
    // Обработка удаления выбранных файлов
    $checked_files = $_POST['files']; // массив выбранных файлов

    foreach ($checked_files as $filename) {
        if (file_exists($filename)) {
            unlink($filename); // удаление файла
        }
    }

    // Редирект на страницу со списком заявок
    header('Location: admin_panel.php');
    exit();
} else {
    // Если не выбрано ни одной заявки для удаления, редирект на страницу со списком заявок
    header('Location: admin_panel.php');
    exit();
}
