<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Форма заявки на участие в конференции</title>
</head>

<body>

    <h1>Форма заявки на участие в конференции</h1>



    <form method="post" action="">
        <label for="firstname">Имя:*</label>
        <input type="text" id="firstname" name="firstname" value="<?= $_POST['firstname'] ?? '' ?>" placeholder="Влад"><br><br>

        <label for="lastname">Фамилия:*</label>
        <input type="text" id="lastname" name="lastname" value="<?= ($_POST['lastname']) ?? '' ?>" placeholder="Никитин"><br><br>

        <label for="email">Электронный адрес:*</label>
        <input type="email" id="email" name="email" value="<?= ($_POST['email']) ?? '' ?>" placeholder="example@gmail.com"><br><br>

        <label for="phone">Телефон для связи:*</label>
        <input type="text" id="phone" name="phone" value="<?= ($_POST['phone']) ?? '' ?>" placeholder="79649950391"><br><br>

        <label for="topic">Интересующая тематика:*</label>
        <select id="topic" name="topic" value="<?= ($_POST['topic'] ?? '') ?>">
            <option selected disabled hidden>Выберите тематику</option>
            <option value="Бизнес" <?php if ($_POST['topic'] === 'Бизнес') {
                                        echo "selected";
                                    } ?>>Бизнес</option>
            <option value="Технологии" <?php if ($_POST['topic'] === 'Технологии') {
                                            echo "selected";
                                        } ?>>Технологии</option>
            <option value="Реклама и Маркетинг" <?php if ($_POST['topic'] === 'Реклама и Маркетинг') {
                                                    echo "selected";
                                                } ?>>Реклама и Маркетинг</option>
        </select>

        <br><br>

        <label for="payment">Предпочитаемый метод оплаты:*</label>
        <select id="payment" name="payment">
            <option selected disabled hidden>Выберите метод оплаты</option>
            <option value="WebMoney" <?php if ($_POST['payment'] === 'WebMoney') {
                                            echo "selected";
                                        } ?>>WebMoney</option>
            <option value="Яндекс.Деньги" <?php if ($_POST['payment'] === 'Яндекс.Деньги') {
                                                echo "selected";
                                            } ?>>Яндекс.Деньги</option>
            <option value="PayPal" <?php if ($_POST['payment'] === 'PayPal') {
                                        echo "selected";
                                    } ?>>PayPal</option>
            <option value="Кредитная карта" <?php if ($_POST['payment'] === 'Кредитная карта') {
                                                echo "selected";
                                            } ?>>Кредитная карта</option>
        </select><br><br>

        <label for="newsletter">Получать рассылку о конференции:</label>
        <input type="checkbox" id="newsletter" name="newsletter" value="yes" <?php if (isset($_POST['newsletter'])) echo "checked='checked'"; ?>><br><br>

        <input type="submit" value="Отправить заявку">

    </form>



</body>



</html>

<!-- <?php
        // Функция для отображения выбора пользователя при ошибке отправки формы
        function check_selection($var, $option)
        {
            if (isset($_POST[$option])) {
                return $_POST[$option];
            } else {
                if ($var == 'text') {
                    if ($option == 'topic') {
                        return 'Выберите тему';
                    } else
                        return 'Выберите метод оплаты';
                } else {
                    return;
                }
            }
        }

        ?> -->


<?php

if ($_POST) {
    // Получение данных из формы
    $firstname = strip_tags($_POST['firstname']);
    // $firstname = preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', $firstname);
    $lastname = strip_tags($_POST['lastname']);
    $email = strip_tags($_POST['email']);
    $phone = $_POST['phone'];
    $topic = $_POST['topic'];
    $payment = $_POST['payment'];
    $newsletter = isset($_POST['newsletter']) ? 'Да' : 'Нет';

    if (!ctype_digit($phone) && (!empty($phone))) {
        echo "Номер может состоять только из цифр!";
        exit;
    } elseif (iconv_strlen($phone) < 11 && (!empty($phone))) {
        echo "Номер должен состоять из 11 цифр!";
        exit;
    }
    // Проверка заполненности всех полей
    if (empty($firstname) || empty($lastname) || empty($email) || empty($phone) || empty($topic) || empty($payment)) {
        echo "Ошибка: вы не заполнили обязательные поля, отмеченные ' * ' ";
        exit;
    }
    // Генерируем уникальное имя файла и создаем директорию, если её нет
    if (!is_dir('form_data')) {
        mkdir('form_data', true);
    }
    $filename = 'form_data/' . date('Ymd_His') . '' . $firstname . '' . $lastname . rand(0, 9999) . '.txt';

    // Создаем файл и записываем данные в него

    $data = $firstname . '|' . $lastname . '|' . $email . '|'
        . $phone . '|' . $topic . '|' . $payment . '|' . $newsletter . "\n";
    file_put_contents($filename, $data, FILE_APPEND);

    // Отправка сообщения об успешной отправке заявки пользователю
    $_POST = array();
    header("Location: success.php");
}

?>