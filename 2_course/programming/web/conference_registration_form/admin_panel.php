<html>

<head>
    <title>Администраторская панель</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        tr:hover {
            background-color: #f5f5f5;
        }

        .delete-btn {
            background-color: #f44336;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            float: right;
        }
    </style>
</head>

<body>

    <h2>Список заявок:</h2>

    <form method="post" action="delete.php">
        <table>
            <thead>
                <tr>
                    <th></th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Email</th>
                    <th>Телефон</th>
                    <th>Тематика</th>
                    <th>Оплата</th>
                    <th>Получать рассылку</th>
                    <th>Дата</th>
                </tr>
            </thead>
            <tbody>
                <?php
                // Получаем список файлов с заявками
                $files = glob('form_data/*.txt');
                if ($files) {
                    foreach ($files as $file) {
                        // Считываем данные из файла

                        $data = file_get_contents($file);
                        $fields = explode("|", $data);
                        $name = trim($fields[0]);
                        $surname = trim($fields[1]);
                        $email = trim($fields[2]);
                        $phone = trim($fields[3]);
                        $topic = trim($fields[4]);
                        $payment = trim($fields[5]);
                        $mailing = trim($fields[6]);
                        $date = date('Y-m-d H:i:s', filemtime($file));

                        // Отображаем данные в таблице
                        echo "<tr>";
                        echo "<td><input type='checkbox' name='files[]' value='$file'></td>";
                        echo "<td>$name</td>";
                        echo "<td>$surname</td>";
                        echo "<td>$email</td>";
                        echo "<td>$phone</td>";
                        echo "<td>$topic</td>";
                        echo "<td>$payment</td>";
                        echo "<td>$mailing</td>";
                        echo "<td>$date</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr><td colspan='9'>Нет заявок</td></tr>";
                }
                ?>
            </tbody>
        </table>
        <input type="submit" name="delete" value="Удалить" class="delete-btn">
    </form>

</body>

</html>