from math import inf

from packages.ensure.ensure_file import ensure
from sle.sle import sle_answer


def linear_interpolation(coordinate_0, coordinate_1, x):
    """Линейная интерполяция"""
    ensure_is_x_in_range(coordinate_0, coordinate_1, x)
    return approximation(coordinate_0, coordinate_1, x)


def linear_extrapolation(coordinate_0, coordinate_1, x):
    """Линейная экстраполяция"""
    ensure_is_x_not_in_range(coordinate_0, coordinate_1, x)
    return approximation(coordinate_0, coordinate_1, x)


def piecewise_linear_interpolation(list_x, data_xy):
    for i in range(len(list_x)):
        ensure_is_x_in_range(min(data_xy), max(data_xy), list_x[i])
    return piecewise_approximation(list_x, data_xy)


def piecewise_linear_extrapolation(list_x, data_xy):
    for i in range(len(list_x)):
        ensure_is_x_not_in_range(min(data_xy), max(data_xy), list_x[i])
    return piecewise_approximation(list_x, data_xy)


def piecewise_approximation(list_x, data_xy):
    tochki = []
    a_b = create_a_and_b_for_equations(data_xy)
    intervals = create_intervals(data_xy)
    for x in list_x:
        for i in range(len(intervals)):
            if intervals[i][0] <= x < intervals[i][1]:
                y = a_b[i][0] * x + a_b[i][1]
                tochki.append(y)
    return tochki


def approximation(coordinate_0, coordinate_1, x=0.0):
    """Аппроксимация"""
    matrix_a = create_a(coordinate_0, coordinate_1)
    matrix_b = create_b(coordinate_0, coordinate_1)
    sle = sle_answer(matrix_a, matrix_b, True, False)
    roots = [column[-1] for column in sle]
    a, b = roots[0], roots[1]
    return (a * x, a * x + b), x, a * x + b, a, b


def create_a_and_b_for_equations(data_xy):
    """Нахождение коэффициетов a и b уравнений"""
    return [approximation(data_xy[i], data_xy[i + 1])[3:5] for i in range(len(data_xy) - 1)]


def create_intervals(data_xy):
    """Создание интервалов для использования уравнений"""
    intervals = [[data_xy[i][0], data_xy[i + 1][0]] for i in range(len(data_xy) - 1)]
    intervals[0][0], intervals[-1][1] = -inf, inf
    return intervals


def lagrange_polynomial(data, step=0.1):
    """Полином Лагранжа"""
    tochki = []
    x = min(data)[0]
    n = len(data)
    while x <= max(data)[0]:
        lxy = []
        for i in range(n):
            xi = data[i][0]
            lxy += lagrange_calc_lx(xi, i, n, data, x)
        tochki.append((x, sum(lxy)))
        x += step
    return tochki


def lagrange_calc_lx(xi, i, n, data, x):
    """Подсчёт базисного полинома"""
    l = 1
    lx_list = []
    for j in range(n):
        if i == j:
            continue
        xj, yj = data[j]
        l *= float(x - xj) / float(xi - xj)
    lx_list.append(data[i][1] * l)
    return lx_list


def ensure_is_x_in_range(coordinate_0, coordinate_1, x):
    """Проверка на нахождение икса в диапазоне иксов координат"""
    ensure(is_x_in_range(coordinate_0, coordinate_1, x), 'x не в диапазоне')


def ensure_is_x_not_in_range(coordinate_0, coordinate_1, x):
    """Проверка на нахождение икса не в диапазоне иксов координат"""
    ensure(not is_x_in_range(coordinate_0, coordinate_1, x), 'x в диапазоне')


def is_x_in_range(coordinate_0, coordinate_1, x):
    """Проверка. Икс в диапазоне иксов координат"""
    return min(coordinate_0[0], coordinate_1[0]) <= x <= max(coordinate_0[0], coordinate_1[0])


def create_a(*coordinates):
    """Создание матрицы А"""
    return [[coordinate[0], 1] for coordinate in coordinates]


def create_b(*coordinates):
    """Создание матрицы b"""
    return [[coordinate[1]] for coordinate in coordinates]
