import interpolation.interpolation as interp


def test_linear_interpolation():
    """Тест линейной интерполяции"""
    pa = (3, 3)
    pb = (7, 7)
    exp = (5.0, 5.0)
    res, x, y, a, b = interp.linear_interpolation(pa, pb, 5)
    assert res == exp


def test_piecewise_linear_interpolation():
    """Тест кусочно-линейной интерполяции"""
    DATA_XY = [(0, -1),
               (2, 0.2),
               (3, 0.5),
               (3.5, 0.8)]

    XY = [1,
          3.2]
    exp = [-0.4, 0.6199999999999999]
    res = interp.piecewise_linear_interpolation(XY, DATA_XY)
    assert res == exp


def test_piecewise_linear_extrapolation():
    """Тест кусочно-линейной экстраполяции"""
    data_xy = [(0, 0),
               (5, 5),
               (10, 5)]
    x = [-2,
         17]
    exp = [-2.0, 5.0]
    res = interp.piecewise_linear_extrapolation(x, data_xy)
    assert res == exp


def test_lagrange_polynomial():
    """Тест полинома Лагранжа"""
    data_xy = [(1, 2),
               (3, 4),
               (3.5, 3),
               (6, 7)]
    exp = ((1, 2.0), (1.5, 4.12), (2.0, 4.92), (2.5, 4.76), (3.0, 4.0), (3.5, 3.0), (4.0, 2.12), (4.5, 1.72),
           (5.0, 2.16), (5.5, 3.8), (6.0, 7.0))
    res = interp.lagrange_polynomial(data_xy, 0.5)
    round_res = []
    for i in res:
        j_list = []
        for j in i:
            j_list.append(round(j, 2))
        round_res.append(tuple(j_list))
    assert tuple(round_res) == exp
