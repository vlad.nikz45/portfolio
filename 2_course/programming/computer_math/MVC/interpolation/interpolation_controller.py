# from packages.view.view_module import show_array, show_error, show_info
from interpolation.interpolation import linear_interpolation, linear_extrapolation, piecewise_linear_interpolation, \
    piecewise_linear_extrapolation, lagrange_polynomial
from packages.matplotlib.show_2d_figure import show_2d_figure_result_matplotlib
from packages.try_calc.try_calc_file import try_calculate as try_calc
from packages.view.show import show_error, show_info, show_array
from sle.sle_view import show_info, show_error
from vector.vector_view import show_array

# DATA_XY = [(0, 0),
#            (5, 5),
#            (10, 5)]
# X = [-2,
#      17]


"""Данные для кусочно-линейной интерполяции"""
# DATA_XY = [(0, -1),
#                (2, 0.2),
#                (3, 0.5),
#                (3.5, 0.8)]
#
# X = [1,3.2]
"""Данные для кусочно-линейной экстраполяции"""
DATA_XY = [(0, 0),
           (5, 5),
           (10, 5)]
X = [-2,
     17]
# DATA_XY = [(1, 2),
#            (3, 4),
#            (3.5, 3),
#            (6, 7)]
# X = [-1.5,
#       3,
#       2,
#       5,
#       9]


COORDINATE_A = (2, 5)

COORDINATE_B = (6, 9)

X_0 = 3

X_1 = 12


def linear_interpolation_action():
    """Решение линейной интерполяции. Действие."""
    coordinate_a, coordinate_b, x1 = COORDINATE_A, COORDINATE_B, X_0
    point1, x1, y1, a1, b1 = try_calc(linear_interpolation, coordinate_a, coordinate_b, x1)
    show_array(point1, 'Решение линейной интерполяции')
    show_info(f'y = ax + b: \n{y1} = {a1} * {x1} + {b1}')
    list_x_vectors, list_y_vectors = convert_data_x_y(coordinate_a, coordinate_b)
    list_x_points, list_y_points = convert_data_x_y(point1)
    show_2d_figure_result_matplotlib(list_x_vectors, list_y_vectors, list_x_points, list_y_points, 'o')


def convert_data_x_y(*coords):
    data_x = []
    data_y = []
    for el in coords:
        if type(el) == list:
            for i in range(len(el)):
                data_x.append(el[i][0])
                data_y.append(el[i][1])
        else:
            data_x.append(el[0])
            data_y.append(el[1])
    return data_x, data_y


def linear_extrapolation_action():
    """Решение линейной экстраполяции. Действие."""
    coordinate_a, coordinate_b, x1 = COORDINATE_A, COORDINATE_B, X_1  # Получение данных для расчетов
    point1, x1, y1, a1, b1 = try_calc(linear_extrapolation, coordinate_a, coordinate_b, x1)
    show_array(point1, 'Решение линейной экстраполяции')
    show_info(f'y = ax + b: \n{y1} = {a1} * {x1} + {b1}')
    list_x_vectors, list_y_vectors = convert_data_x_y(coordinate_a, coordinate_b)
    list_x_points, list_y_points = convert_data_x_y(point1)
    try_run_matplotlib(list_x_vectors, list_y_vectors, list_x_points, list_y_points)


def piecewise_linear_interpolation_action():
    """Решение кусочно-линейной интерполяции. Действие."""
    data_xy, list_x = DATA_XY, X  # Получение данных для расчетов
    list_x_vectors, list_y_vectors = convert_data_x_y(data_xy)
    points = try_calc(piecewise_linear_interpolation, list_x, data_xy)
    show_array(points, 'y-грики')
    try_run_matplotlib(list_x_vectors, list_y_vectors, list_x, points)


def piecewise_linear_extrapolation_action():
    """Решение кусочно-линейной экстраполяции. Действие."""
    data_xy, list_x = DATA_XY, X  # Получение данных для расчетов
    list_x_vectors, list_y_vectors = convert_data_x_y(data_xy)
    points = try_calc(piecewise_linear_extrapolation, list_x, data_xy)
    try_run_matplotlib(list_x_vectors, list_y_vectors, list_x, points)


def lagrange_polynomial_action():
    """Решение полинома Лагранжа. Действие."""
    data = DATA_XY
    res = try_calc(lagrange_polynomial, data)
    list_x_vectors, list_y_vectors = convert_data_x_y(res)
    list_x_points, list_y_points = convert_data_x_y(data)
    try_run_matplotlib(list_x_vectors, list_y_vectors, list_x_points, list_y_points)


def try_run_matplotlib(list_x_vectors, list_y_vectors, list_x_points, list_y_points):
    """Проверка вызова библиотеки matplotlib"""
    try:
        show_2d_figure_result_matplotlib(list_x_vectors, list_y_vectors, list_x_points, list_y_points, 'o')
    except Exception as err:
        return show_error(err, 'Ошибка при вызове библиотеки matplotlib')


def get_actions():
    """Получение доступных действий"""
    return [linear_interpolation_action, linear_extrapolation_action, piecewise_linear_interpolation_action,
            piecewise_linear_extrapolation_action, lagrange_polynomial_action]
