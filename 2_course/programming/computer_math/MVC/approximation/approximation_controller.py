from approximation.approximation import lsm_matrix_one, lsm_matrix_calc_y_for_x, lsm_matrix_two, linear_approximation, \
    approximation_polynomial_2nd, approximation_polynomial_3nd
from packages.matplotlib.show_2d_figure import show_2d_figure_result_matplotlib
from packages.matplotlib.show_2d_figure import show_2d_figure_result_matplotlib_axline, \
    show_2d_figure_result_matplotlib_with_add_points
from packages.matplotlib.surface_3d import show_3d_figure_result_matplotlib
from packages.matplotlib.try_run import try_run_matplotlib
from packages.try_calc.try_calc_file import try_calculate
from packages.view.show import show_info

A_one = [[2],
         [3]]

B_one = [[4],
         [9]]
A_two = [[2, 3],
         [3, 3],
         [4, 7]]

B_two = [[7],
         [7],
         [3]]
DATA_XY = [[1, 2],
           [3, 4],
           [3.5, 3],
           [6, 7]]

LIST_X = [[1],
          [3],
          [5]]


def lsm_matrix_one_action():
    res, data = try_calculate(lsm_matrix_one, A_one, B_one)
    show_info(f'Ответ: {res}')
    try_run_matplotlib(show_2d_figure_result_matplotlib, data[0], data[1], res,
                       lsm_matrix_calc_y_for_x(A_one, B_one, res[0]), '')


def lsm_matrix_two_action():
    res, data = try_calculate(lsm_matrix_two, A_two, B_two)
    show_info(f'Ответ: {res}')
    try_run_matplotlib(show_3d_figure_result_matplotlib, data[0], data[1], data[2], [res[0]], [res[1]], [res[2]])


def linear_approximation_action():
    res = linear_approximation(DATA_XY, LIST_X)
    show_info(f'Ответ: {res}')
    try_run_matplotlib(show_2d_figure_result_matplotlib_axline, LIST_X, res, LIST_X,
                       [[column[0]] for column in DATA_XY], res,
                       [[column[-1]] for column in DATA_XY], '')


def approximation_polynomial_2nd_action():
    res, data = approximation_polynomial_2nd(DATA_XY, LIST_X)
    show_info(f'Ответ: {res}')
    try_run_matplotlib(show_2d_figure_result_matplotlib_with_add_points, data[0], data[1], LIST_X,
                       [[column[0]] for column in DATA_XY],
                       res, [[column[-1]] for column in DATA_XY], '')


def approximation_polynomial_3nd_action():
    res, data = approximation_polynomial_3nd(DATA_XY, LIST_X)
    show_info(f'Ответ: {res}')
    try_run_matplotlib(show_2d_figure_result_matplotlib_with_add_points, data[0], data[1], LIST_X,
                       [[column[0]] for column in DATA_XY],
                       res, [[column[-1]] for column in DATA_XY], '')


def get_actions():
    """Получение доступных действий"""
    return [lsm_matrix_one_action, lsm_matrix_two_action, linear_approximation_action,
            approximation_polynomial_2nd_action, approximation_polynomial_3nd_action]
