import approximation.approximation as apm


def test_lsm_matrix_one():
    """Тест метода наименьших квадратов в матричном виде с одной неизвестной"""
    a = [[2],
         [3]]
    b = [[4],
         [9]]
    exp = [2.6923076923076925]
    res = apm.lsm_matrix_one(a, b)[0]
    assert res == exp


def test_lsm_matrix_two():
    """Тест метода наименьших квадратов в матричном виде с двумя неизвестными"""
    a = [[2, 3],
         [3, 3],
         [4, 7]]
    b = [[7],
         [7],
         [3]]
    exp = [4.680851063829789, -2.063829787234044, 17.021276595744684]
    res = apm.lsm_matrix_two(a, b)[0]
    assert res == exp


def test_linear_approximation():
    """Тест линейной аппроксимации"""
    data_xy = [(1, 2),
               (3, 4),
               (3.5, 3),
               (6, 7)]
    list_x = [[1],
              [3],
              [5]]
    exp = [[1.660098522167488], [3.6305418719211824], [5.600985221674877]]
    res = apm.linear_approximation(data_xy, list_x)
    assert res == exp


def test_approximation_2nd_polynomial():
    """Тест аппроксимационного полиномома 2-ой степени"""
    data_xy = [[1, 2],
               [3, 4],
               [3.5, 3],
               [6, 7]]
    list_x = [[1],
              [3],
              [5]]
    exp = [[2.0889621087314683], [3.258649093904446], [5.456342668863261]]
    res = apm.approximation_polynomial_2nd(data_xy, list_x)[0]
    assert res == exp


def test_approximation_3nd_polynomial():
    """Тест аппроксимационного полиномома 3-ой степени"""
    data_xy = [[1, 2],
               [3, 4],
               [3.5, 3],
               [6, 7]]
    list_x = [[1],
              [3],
              [5]]
    exp = [[2.0000000000000515], [3.9999999999995257], [2.1600000000021087]]
    res = apm.approximation_polynomial_3nd(data_xy, list_x)[0]
    assert res == exp
