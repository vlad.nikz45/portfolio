from interpolation.interpolation import create_a, create_b
from matrix.matrix import transposition_matrix, multiply_matrix, extended_matrix
from sle.sle import sle_answer


def lsm_matrix_one(a, b):
    new_a = multiply_matrix(transposition_matrix(a), a)
    new_b = multiply_matrix(transposition_matrix(a), b)
    x = [b / a for b, a in zip(new_b[0], new_a[0])]
    data = lsm_matrix_calc_xy_one(a, b, x[0])
    return x, data


def lsm_matrix_calc_xy_one(a, b, x, step=0.1):
    c = [[(a * -1) for a in b[0]], [(a * -1) for a in b[1]]]
    extended = extended_matrix(a, c)
    list_x = []
    list_y = []
    x_min = x - 5
    while x_min < (x + 5):
        list_x.append(x_min)
        list_sum = []
        for i in range(len(extended)):
            eps = extended[i][0] * x_min + extended[i][1]
            list_sum.append(eps * eps)
        list_y.append(sum(list_sum))
        x_min += step
    return list_x, list_y


def lsm_matrix_calc_xy_two(a, b, x, y, step=0.8):
    c = [[(a * -1) for a in b[0]], [(a * -1) for a in b[1]], [(a * -1) for a in b[2]]]
    extended = extended_matrix(a, c)
    list_x = []
    list_y = []
    list_z = []
    x_min = x - 5
    while x_min < (x + 5):
        y_min = y - 5
        while y_min < (y + 5):
            list_sum = []
            for i in range(len(extended)):
                eps = extended[i][0] * x_min + extended[i][1] * y_min + extended[i][2]
                list_sum.append(eps * eps)
            list_z.append(sum(list_sum))
            list_x.append(x_min)
            list_y.append(y_min)
            y_min += step
        x_min += step
    return list_x, list_y, list_z


def lsm_matrix_calc_y_for_x(a, b, x):
    c = [[(a * -1) for a in b[0]], [(a * -1) for a in b[1]]]
    list_y = []
    extended = extended_matrix(a, c)
    list_sum = []
    for i in range(len(extended)):
        eps = extended[i][0] * x + extended[i][1]
        list_sum.append(eps * eps)
    list_y.append(sum(list_sum))
    return list_y


def lsm_matrix_calc_z_for_xy(a, b, x, y):
    c = [[(a * -1) for a in b[0]], [(a * -1) for a in b[1]], [(a * -1) for a in b[2]]]
    list_z = []
    extended = extended_matrix(a, c)
    list_sum = []
    for i in range(len(extended)):
        eps = extended[i][0] * x + extended[i][1] * y + extended[i][2]
        list_sum.append(eps * eps)
    list_z.append(sum(list_sum))
    return list_z


def lsm_matrix_two(a, b):
    new_a = multiply_matrix(transposition_matrix(a), a)
    new_b = multiply_matrix(transposition_matrix(a), b)
    sle = sle_answer(new_a, new_b, False, False)
    sle_x = [column[-1] for column in sle]
    z = lsm_matrix_calc_z_for_xy(a, b, sle_x[0], sle_x[1])
    data = lsm_matrix_calc_xy_two(a, b, sle_x[0], sle_x[1])
    return sle_x + z, data


def polynomial_2nd(x, min_x):
    """Формула для полинома второго порядка"""
    return x[0][0] * min_x * min_x + x[1][0] * min_x + x[2][0]


def polynomial_3nd(x, min_x):
    """Формула для полинома третьего порядка"""
    return x[0][0] * min_x * min_x * min_x + x[1][0] * min_x * min_x + x[2][0] * min_x + x[3][0]


def polynomial(func, x_list, x, step):
    list_x = []
    list_y = []
    x_min = min(x_list[0]) - 2
    while x_min < max(x_list[0]) + 7:
        list_x.append(x_min)
        y = func(x, x_min)
        list_y.append(y)
        x_min += step
    return list_x, list_y


def linear_approximation(data_xy, list_x):
    a = create_a(*data_xy)
    sle_x = calc_x(data_xy, a)
    matrix_x = create_a(*list_x)
    list_y = multiply_matrix(matrix_x, sle_x)
    return list_y


def calc_x(data_xy, a):
    new_a = multiply_matrix(transposition_matrix(a), a)
    b = create_b(*data_xy)
    new_b = multiply_matrix(transposition_matrix(a), b)
    sle = sle_answer(new_a, new_b, False, False)
    return [[column[-1]] for column in sle]


def calc_y(list_x, sle_x):
    matrix_x = [[i[0] * i[0], *i] for i in create_a(*list_x)]
    return multiply_matrix(matrix_x, sle_x)


def approximation_polynomial_2nd(data_xy, list_x):
    a = [[i[0] * i[0], *i] for i in create_a(*data_xy)]
    sle_x = calc_x(data_xy, a)
    list_y = calc_y(list_x, sle_x)
    data = polynomial(polynomial_2nd, list_x, sle_x, 0.1)
    return list_y, data


def approximation_polynomial_3nd(data_xy, list_x):
    a = [[i[0] * i[0] * i[0], i[0] * i[0], *i] for i in create_a(*data_xy)]
    sle_x = calc_x(data_xy, a)
    matrix_x = [[i[0] * i[0] * i[0], i[0] * i[0], *i] for i in create_a(*list_x)]
    list_y = multiply_matrix(matrix_x, sle_x)
    data = polynomial(polynomial_3nd, list_x, sle_x, 0.1)
    return list_y, data
