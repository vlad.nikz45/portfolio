from math import pi


def factorial(n):
    """Нахождение факториала числа"""
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)


def calc_e_x(x, i):
    """Рассчёт члена ряда e^x"""
    return x ** i / factorial(i)


def maclaurin_series(n, func, x_min=-5, x_max=6, step=0.1):
    """Разложение в ряд Маклорена"""
    list_x = []
    list_y = []
    while x_min <= x_max:
        series = []
        for i in range(n + 1):
            series.append(func(x_min, i))
        if func != calc_arc_cos:
            list_x.append(x_min)
            list_y.append(sum(series))
        else:
            list_x.append(x_min)
            list_y.append(pi / 2 - sum(series))

        x_min += step
    return list_x, list_y


def calc_cos(x, i):
    """Рассчёт члена ряда cos(x)"""
    return ((x ** (2 * i)) * (-1) ** i) / factorial(2 * i)


def calc_sin(x, i):
    """Рассчёт члена ряда sin(x)"""
    return ((x ** (2 * i + 1)) * (-1) ** i) / factorial(2 * i + 1)


def calc_arc_sin(x, i):
    """Рассчёт члена ряда arc_sin(x)"""
    return (factorial(2 * i) * (x ** (2 * i + 1))) / ((4 ** i) * factorial(i) * factorial(i) * (2 * i + 1))


def calc_arc_cos(x, i):
    """Рассчёт члена ряда arc_cos(x)"""
    return calc_arc_sin(x, i)
