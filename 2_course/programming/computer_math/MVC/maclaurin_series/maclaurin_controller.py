from maclaurin_series.maclaurin import maclaurin_series, calc_e_x, calc_cos, calc_sin, calc_arc_sin, calc_arc_cos
from packages.matplotlib.show_2d_figure import show_two_2d_figure_result_matplotlib
from packages.try_calc.try_calc_file import try_calculate

n = 2
n2 = 5
n3 = 0

add_n = 10


def maclaurin_series_e_x_action():
    """Разложение в ряд Маклорена e^x"""
    list_x, list_y = try_calculate(maclaurin_series, n, calc_e_x)
    add_list_x, add_list_y = try_calculate(maclaurin_series, add_n, calc_e_x)
    show_two_2d_figure_result_matplotlib(add_list_x, add_list_y, list_x, list_y, '')


def maclaurin_series_cos_action():
    """Разложение в ряд Маклорена cos(x)"""
    list_x, list_y = try_calculate(maclaurin_series, n2, calc_cos)
    add_list_x, add_list_y = try_calculate(maclaurin_series, add_n, calc_cos)
    show_two_2d_figure_result_matplotlib(add_list_x, add_list_y, list_x, list_y, '')


def maclaurin_series_sin_action():
    """Разложение в ряд Маклорена sin(x)"""
    list_x, list_y = try_calculate(maclaurin_series, n2, calc_sin)
    add_list_x, add_list_y = try_calculate(maclaurin_series, add_n, calc_sin)
    show_two_2d_figure_result_matplotlib(add_list_x, add_list_y, list_x, list_y, '')


def maclaurin_series_arc_sin_action():
    """Разложение в ряд Маклорена arc_sin(x)"""
    list_x, list_y = try_calculate(maclaurin_series, n3, calc_arc_sin)
    add_list_x, add_list_y = try_calculate(maclaurin_series, add_n, calc_arc_sin)
    show_two_2d_figure_result_matplotlib(add_list_x, add_list_y, list_x, list_y, '')


def maclaurin_series_arc_cos_action():
    """Разложение в ряд Маклорена arc_cos(x)"""
    list_x, list_y = try_calculate(maclaurin_series, n, calc_arc_cos)
    add_list_x, add_list_y = try_calculate(maclaurin_series, add_n, calc_arc_cos)
    show_two_2d_figure_result_matplotlib(add_list_x, add_list_y, list_x, list_y, '')


def get_actions():
    """Получение доступных действий"""
    return [maclaurin_series_e_x_action, maclaurin_series_cos_action, maclaurin_series_sin_action,
            maclaurin_series_arc_sin_action, maclaurin_series_arc_cos_action]
