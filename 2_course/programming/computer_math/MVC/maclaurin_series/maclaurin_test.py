import maclaurin_series.maclaurin as mc


def test_maclaurin_series_expansion_ex():
    """Тест разложения в ряд Маклорена e^x"""
    n = 25
    x = 1
    exp_y = [2.7182818284590455]
    list_x, list_y = mc.maclaurin_series(n, mc.calc_e_x, x, x)
    assert exp_y == list_y


def test_maclaurin_series_sin():
    """Тест разложения в ряд Маклорена sin(x)"""
    n = 25
    x = 1
    exp_y = [0.8414709848078965]
    list_x, list_y = mc.maclaurin_series(n, mc.calc_sin, x, x)
    assert exp_y == list_y


def test_maclaurin_series_cos():
    """Тест разложения в ряд Маклорена cos(x)"""
    n = 25
    x = 1
    exp_y = [0.5403023058681397]
    list_x, list_y = mc.maclaurin_series(n, mc.calc_cos, x, x)
    assert exp_y == list_y


def test_maclaurin_series_arc_cos():
    """Тест разложения в ряд Маклорена arc_cos(x)"""
    n = 25
    x = 0.5
    exp_y = [1.0471975511965974]
    list_x, list_y = mc.maclaurin_series(n, mc.calc_arc_cos, x, x)
    assert exp_y == list_y


def test_maclaurin_series_arc_sin():
    """Тест разложения в ряд Маклорена arc_sin(x)"""
    n = 25
    x = 0.5
    exp_y = [0.523598775598299]
    list_x, list_y = mc.maclaurin_series(n, mc.calc_arc_sin, x, x)
    assert exp_y == list_y
