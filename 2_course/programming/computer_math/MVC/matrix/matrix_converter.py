def str_to_float_array(s):
    # Преобразование строки в массив. Converter.
    lst = s.split(',')
    a = [float(x) for x in lst]
    return a


def str_to_float_number(s):
    # Преобразование строки в число (float)
    return float(s)


def str_to_int_number(s):
    # Преобразование строки в число (int)
    return int(s)
