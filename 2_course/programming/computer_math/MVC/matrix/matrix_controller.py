from matrix.matrix import sum_matrix, multiply_matrix, sub_matrix, transposition_matrix, multiply_scalar_matrix, \
    index_of_row_matrix, index_of_column_matrix, swap_rows_matrix, multiply_raw_matrix_on_scalar, \
    add_raw_multiply_scalar_matrix, sub_raw_multiply_scalar_matrix
from matrix.matrix_converter import str_to_float_number, str_to_int_number
from matrix.matrix_view import show_matrix, show_error, show_raw_matrix

m1 = [[10, 4, 3], [22, 3, 4]]
m2 = [[2, 2, 2],
      [3, 3, 3]]


def sum_matrix_action():
    res = safe_exec(sum_matrix, m1, m2)

    show_matrix(res, 'Сумма матриц')


def sub_matrix_action():
    res = safe_exec(sub_matrix, m1, m2)

    show_matrix(res, 'Сумма матриц')


def transposition_matrix_action():
    res = safe_exec(transposition_matrix, m1)

    show_matrix(res, 'Транспозиция матрицы')


def multiply_matrix_action():
    res = safe_exec(multiply_matrix, m1, m2)

    show_matrix(res, 'Умножение матриц')


def multiply_scalar_matrix_action():
    res = safe_exec(multiply_scalar_matrix, m1, input_number('Введите скаляр: '))

    show_matrix(res, 'Умножение матрицы на скаляр')


def index_of_raw_matrix_action():
    res = safe_exec(index_of_row_matrix, m1, input_number('Введите индекс строки: ', int),input_copy_question())

    show_raw_matrix(res, 'Строка матрицы по введенному индексу: ')


def index_of_column_matrix_action():
    res = safe_exec(index_of_column_matrix, m1, input_number('Введите индекс столбца: ', int))

    show_raw_matrix(res, 'Столбец по введенному индексу')


def swap_rows_matrix_action():
    res = safe_exec(swap_rows_matrix, m1, input_number('Введите индекс первой строки: ', int),
                    input_number('Введите индекс второй строки: ', int),input_copy_question())

    show_matrix(res, 'Матрица после смены строк')


def multiply_raw_matrix_on_scalar_action():
    res = safe_exec(multiply_raw_matrix_on_scalar, m1, input_number('Введите индекс строки: ', int),
                    input_number('Введите скаляр: ', float),input_copy_question())

    show_matrix(res, 'Матрица после смены строк')


def add_raw_multiply_scalar_matrix_action():
    res = safe_exec(add_raw_multiply_scalar_matrix, m1, input_number('Введите индекс первой строки: ', int),
                    input_number('Введите индекс второй строки: ', int), input_number('Введите скаляр: ', int),input_copy_question())

    show_matrix(res, 'Прибавление строки одной матрицы к другой, умноженной на скаляр')


def sub_raw_multiply_scalar_matrix_action():
    res = safe_exec(sub_raw_multiply_scalar_matrix, m1, input_number('Введите индекс первой строки: ', int),
                    input_number('Введите индекс второй строки: ', int), input_number('Введите скаляр: ', int), input_copy_question())

    show_matrix(res, 'Вычитание от одной строки матрицы другой, умноженной на скаляр')


def input_number(option='Введите число', type_num=float):
    try:
        s = input(option)
        if type_num == int:
            a = str_to_int_number(s)
        else:
            a = str_to_float_number(s)
    except Exception as error:
        show_error(error, 'Ошибка при вводе числа, попробуйте еще раз')
        return input_number(option, type_num)
    return a
def input_copy_question():
    answer = input('Хотите ли вы сделать копию матрицы? (Введите "y" если да, "n" если нет) : ')
    if answer == 'y':
        return True
    if answer == 'n':
        return False
    if answer == '':
        return True
    else:
        print('Вы что-то неправильно ввели, попробуйте еще раз.')
        input_copy_question()


def safe_exec(func, *m):
    try:
        res = func(*m)
    except Exception as error:
        return show_error(error, "Ошибка при вычислении")
    return res


def get_actions():
    return [sum_matrix_action, multiply_matrix_action, sub_matrix_action, transposition_matrix_action,
            multiply_scalar_matrix_action, index_of_raw_matrix_action, index_of_column_matrix_action,
            swap_rows_matrix_action,
            multiply_raw_matrix_on_scalar_action, add_raw_multiply_scalar_matrix_action,
            sub_raw_multiply_scalar_matrix_action]
