import matplotlib.pyplot as plt


def show_matrix(res, msg):
    # Вывод массива на экран. View
    if res is not None:
        print(msg)
        for i in range(len(res)):
            for j in range(len(res[i])):
                print((res[i][j]), end="\t")
            print()


def show_error(err, msg):
    # Вывод ошибки на экран. View.
    print(msg)
    print(err)


def show_raw_matrix(res,msg):
    if res is not None:
        print(msg)
        print(res)

