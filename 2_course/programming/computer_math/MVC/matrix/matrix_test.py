import matrix.matrix as mx


def test_sum():
    a = [[1, 2, 2], [3, 3, 2]]
    b = [[4, 8, 2], [0, 0, -1]]
    exp = [[5, 10, 4], [3, 3, 1]]
    res = mx.sum_matrix(a, b)
    assert res == exp


def test_transposition():
    a = [[2, 3, 5], [5, 7, 6], [5, 7, 6]]
    exp = [[2, 5, 5], [3, 7, 7], [5, 6, 6]]
    res = mx.transposition_matrix(a)
    assert res == exp


def test_multiply_scalar_matrix():
    a = [[1, 2], [3, 4]]
    b = 5
    exp = [[5, 10], [15, 20]]
    res = mx.multiply_scalar_matrix(a, b)
    assert res == exp


def test_multiply_matrix():
    a = [[1, 2], [3, 4], [5, 6]]
    b = [[2, 3, 2], [4, 5, 2]]
    exp = [[10, 13, 6], [22, 29, 14], [34, 45, 22]]
    res = mx.multiply_matrix(a, b)
    assert res == exp


def test_index_of_row_matrix():
    a = [[7, 5, 1], [52, 7, 11], [1, 2, 3]]
    b = 1
    exp = [52, 7, 11]
    res = mx.index_of_row_matrix(a, b)
    assert res == exp


def test_index_of_column():
    a = [[10, 13, 6], [22, 29, 14], [34, 45, 22]]
    b = 2
    exp = [6, 14, 22]
    res = mx.index_of_column_matrix(a, b)
    assert res == exp


def test_swap_rows_matrix():
    a = [[3, 2, 1], [2, 2, 2], [0, 0, 0]]
    b = 0
    c = 2
    exp = [[0, 0, 0], [2, 2, 2], [3, 2, 1]]
    res = mx.swap_rows_matrix(a, b, c)
    assert res == exp


def test_multiply_row_matrix_on_scalar():
    a = [[10, 13, 6], [22, 29, 14], [34, 45, 22]]
    b = 1
    c = 2
    exp = [[10, 13, 6], [44, 58, 28], [34, 45, 22]]
    res = mx.multiply_raw_matrix_on_scalar(a, b, c)
    assert res == exp


def test_add_raw_multiply_scalar_matrix():
    a = [[10, 4, 3], [22, 3, 4]]
    b = 0
    c = 1
    d = 5
    exp = [[120, 19, 23], [22, 3, 4]]
    res = mx.add_raw_multiply_scalar_matrix(a, b, c, d)
    assert res == exp


def test_sub_row_multiply_scalar_matrix():
    a = [[1, 6, 3], [6, 3, 4]]
    b = 0
    c = 1
    d = 5
    exp = [[-29, -9, -17], [6, 3, 4]]
    res = mx.sub_raw_multiply_scalar_matrix(a, b, c, d)
    assert res == exp
