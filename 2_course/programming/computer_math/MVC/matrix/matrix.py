import copy

from vector.vector import add, sub, multiply_scalar


def get_row(matrix, column, doCopy=True):
    """Получение строки"""
    if doCopy:
        copy_matrix = deep_copy(matrix)
        return copy_matrix[column]
    else:
        return matrix[column]


def unit_matrix(len):
    """Генерация единичной матрицы"""
    unit_matrix = [[int(i == j) for j in range(len)] for i in range(len)]
    return unit_matrix


def inverse_matrix(res,b):
    return [column[-len(b):] for column in res]

def check_equal_len_rows(a):
    """Проверка равности строк исходной матрицы"""
    return all(len(i) == len(a[0]) for i in a)


def ensure_equal_length_matrix(m1, m2):
    """Выкидывает ошибку, если длина матриц не одинакова"""
    if check_length_matrix(m1, m2):
        return True
    else:
        raise Exception("Для данной операции длина матриц должна быть одинаковой")


def ensure_equal_rows_columns(m1, m2):
    """Выкидывает ошибку, если число строк одной матрицы не совпадает с числом столбцов другой"""
    if check_rows_columns_matrix(m1, m2):
        return True
    else:
        raise Exception("Число строк одной матрицы должно совпадать с числом стоблцов другой")


def multiply_scalar_matrix(m1, scalar):
    """Умножение матрицы на скаляр"""
    return [multiply_scalar(raw, scalar) for raw in m1]


def sum_matrix(m1, m2):
    """Сложение матриц"""
    if ensure_equal_length_matrix(m1, m2):
        return [add(x, y) for x, y in zip(m1, m2)]


def sub_matrix(m1, m2):
    """Вычитание матриц"""
    if ensure_equal_length_matrix(m1, m2):
        return [sub(x, y) for x, y in zip(m1, m2)]


def transposition_matrix(m1):
    """Транспозиция матрицы"""
    return [list(i) for i in zip(*m1)]


def multiply_matrix(m1, m2):
    """Умножение матриц"""
    if ensure_equal_rows_columns(m1, m2):
        return [[sum(x * y for x, y in zip(m1_r, m2_c)) for m2_c in zip(*m2)] for m1_r in m1]


def check_length_matrix(m1, m2):
    """Проверка длин матриц"""
    return len(m1) == len(m2)


def index_of_column_matrix(m1, index):
    """Вывод столбца матрицы по индексу"""
    return transposition_matrix(m1)[index]


def index_of_row_matrix(m1, index, doCopy=True):
    """Вывод строки матрицы по индексу"""
    if doCopy:
        out = deep_copy(m1)
    else:
        out = m1
    return out[index]


def swap_rows_matrix(m1, start, out, doCopy=True):
    """Перестановка строк матрицы местами"""
    if doCopy:
        matrix_out = copy.deepcopy(m1)
    else:
        matrix_out = m1
    matrix_out[start], matrix_out[out] = matrix_out[out], matrix_out[start]
    return matrix_out


def extended_matrix(a, b, doCopy=True):
    """Создание единой матрицы из исходных данных СЛАУ"""
    if doCopy:
        copy_a = copy.deepcopy(a)
        copy_b = copy.deepcopy(b)
    else:
        copy_a = a
        copy_b = b
    for i in range(len(b)):
        copy_a[i].extend(copy_b[i])
    return copy_a


def add_raw_multiply_scalar_matrix(m1, start, out, scalar, doCopy=True):
    """Прибавление строки одной матрицы к другой, умноженной на скаляр"""
    if doCopy:
        matrix_out = deep_copy(m1)
    else:
        matrix_out = m1
    matrix_out[start] = add([i * scalar for i in matrix_out[out]], matrix_out[start])
    return matrix_out


def sub_raw_multiply_scalar_matrix(m1, start, out, scalar, doCopy=True):
    """Прибавление строки одной матрицы к другой, умноженной на скаляр"""
    if doCopy:
        matrix_out = deep_copy(m1)
    else:
        matrix_out = m1
    matrix_out[start] = sub(matrix_out[start], [i * scalar for i in matrix_out[out]])
    return matrix_out


def multiply_raw_matrix_on_scalar(m1, index, scalar, doCopy=True):
    """Умножение строки матрицы на скаляр"""
    if doCopy:
        matrix_out = deep_copy(m1)
    else:
        matrix_out = m1
    matrix_out[index] = [i * scalar for i in matrix_out[index]]
    return matrix_out


def deep_copy(matrix):
    return copy.deepcopy(matrix)


def check_rows_columns_matrix(m1, m2):
    """Проверка равенства числа столбцов одной матрицы и числа строк другой матрицы"""
    return len(m2) == len([list(i) for i in zip(*m1)])
