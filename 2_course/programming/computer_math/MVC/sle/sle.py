from matrix.matrix import swap_rows_matrix, sub_raw_multiply_scalar_matrix, check_equal_len_rows, multiply_matrix, \
    extended_matrix
from packages.ensure.ensure_file import ensure
from sle.sle_view import logging_print
from vector.vector import division_scalar, check_equal_len


def check_leading_zero(matrix, row, column, logs):
    """Проверка ведущего числа, является ли оно нулем и смена строк"""
    # Проверили ведущее число, является ли оно нулем
    if check_leading_is_zero(matrix, row, column):
        index_new_row = find_row_without_leading_zero(matrix, row, column)
        if index_new_row is None:
            return None
        # Нашли строку без ведущего нуля в этой колонке и меняем строки местами
        if index_new_row > row:
            matrix = swap_rows_matrix(matrix, row, index_new_row)
        if logs == True:
            logging_print(matrix, f'Поменяли местами строки {row + 1} и {index_new_row + 1}', None)
    return matrix


def check_leading_is_zero(matrix, row, index):
    """Проверка, является ли данный ведущий элемент нулем"""
    return matrix[row][index] == 0


def find_row_without_leading_zero(matrix, row, index):
    """Поиск строки без ведущего нуля вниз"""
    for i in range(row, len(matrix)):
        if check_leading_is_zero(matrix, i, index):
            continue
        else:
            return i
    return find_row_without_leading_zero_up(matrix, row, index)


def find_row_without_leading_zero_up(matrix, row, index):
    """Поиск строки без ведущего нуля вниз"""
    for i in range(row - 1, -1, -1):
        if check_leading_is_zero(matrix, i, index):
            continue
        else:
            return i  # Вовзращаем индекс строки
    # Возвращаем None, если такой строки не существует
    return None


def get_element_in_row(matrix, row, column):
    """Получение элемента в строке"""
    return matrix[row][column]


def sle_answer(a, b, doCopy=True, logs=True):
    """Функция решения СЛАУ"""
    ensure(check_equal_len(a, b), 'Длины исходных списков должны совпадать')
    ensure(check_equal_len_rows(a), 'Длины строк матрицы должны быть одинаковы')
    extended_system = extended_matrix(a, b, doCopy)
    if logs:
        logging_print(extended_system, 'Расширение матрицы', None)

    for i in range(len(extended_system)):  # Цикл для приведения матрицы к треугольному виду
        extended_system = check_leading_zero(extended_system, i, i,
                                             logs)  # Проверяем, является ли ведущий элемент нулем
        if extended_system is None:  # Если ведущий элемент ноль и программа не нашла строки в столбце без нуля
            return None

        # Приводим к единице ведущий элемент
        if not check_leading_is_zero(extended_system, i, i):  # Если ведущий элемент не равен нулю, делим
            extended_system[i] = division_scalar(extended_system[i], get_element_in_row(extended_system, i, i))

            if logs:
                logging_print(extended_system, 'Привели к единице ведущий элемент', (i, i))

        # Приводим к нулю остальные элементы столбца, первый цикл идет вниз, второй - вверх
        extended_system = to_zero_column_elements(extended_system, i)

        if logs:
            logging_print(extended_system, 'Занулили элементы в столбце', (i, i))

    return extended_system


def sle_with_reverse_matrix(a, b):
    """Решение СЛАУ с помощью обратной матрицы"""
    return multiply_matrix(a, b)


def to_zero_column_elements(matrix, column):
    """Приведение к нулю элементов столбца"""
    for index_row in range(column + 1, len(matrix)):
        matrix = sub_raw_multiply_scalar_matrix(matrix, index_row, column,
                                                get_element_in_row(matrix, index_row, column))
    for index_row in range(column - 1, -1, -1):
        matrix = sub_raw_multiply_scalar_matrix(matrix, index_row, column,
                                                get_element_in_row(matrix, index_row, column))
    return matrix
