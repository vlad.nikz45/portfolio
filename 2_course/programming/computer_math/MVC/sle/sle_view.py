def print_sle(matrix, msg):
    print(msg)
    for row in range(len(matrix)):
        for col in range(len(matrix[row])):
            print("\t{1:10}{0}".format(" ", matrix[row][col]),
                  end='')
        print("\t")


def print_answer(matrix):
    print('Найденные иксы')
    print([column[-1] for column in matrix])


def show_info(msg):
    print(msg)


def logging_print(matrix, msg, selected):
    print(msg)
    for row in range(len(matrix)):
        for col in range(len(matrix[row])):
            print("\t{1:10}{0}".format(" " if (selected is None or selected != (row, col)) else "*", matrix[row][col]),
                  end='')
        print("\t")


def show_error(err, msg):
    print(msg)
    print(err)
