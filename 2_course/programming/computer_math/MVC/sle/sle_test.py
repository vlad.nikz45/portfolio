import sle.sle as sle


def test_sle_2d():
    a = [[1, 1],
         [2, 1]]
    b = [[1],
         [2]]
    exp = [[1, 0, 1],
           [0, 1, 0]]
    res = sle.sle_answer(a, b, True, False)
    assert res == exp


def test_sle_3d():
    a = [[-1, 2, 6],
         [3, -6, 0],
         [1, 0, 6]]
    b = [[15],
         [-9],
         [5]]

    exp = [[1, 0, 0, -7],
           [0, 1, 0, -2],
           [0, 0, 1, 2]]
    res = sle.sle_answer(a, b, True, False)
    assert res == exp
