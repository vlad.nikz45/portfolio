from matrix.matrix import unit_matrix,inverse_matrix
from sle.sle import sle_answer, sle_with_reverse_matrix
from sle.sle_view import print_sle, show_error, print_answer, show_info

a = [[-1, 2, 6],
     [3, -6, 0],
     [1, 0, 6]]

b = [[15],
     [-9],
     [5]]


def try_res(function, *args):
    try:
        res = function(*args)
    except ValueError as error:
        return show_error(error, 'Ошибка со значениями данных')
    except IndexError as error:
        return show_error(error, 'Ошибка при вычислении, выход за границы списка')
    if res is None:
        return print_sle(a, 'У данной матрицы не существует решений')
    return res


def sle_answer_action():
    res = try_res(sle_answer, a, b, input_user_true_or_false(
        'Хотите ли вы сделать копию исходных данных? (Введите "y" если да, "n" если нет) : '),
                  input_user_true_or_false(
                      'Хотите ли вы видеть действия программы над СЛАУ?(логи) (Введите "y" если да, "n" если нет) : '))
    return print_sle(res, 'Итоговая матрица: '), print_answer(res)


def reverse_action():
    unit = unit_matrix(len(a))
    res = try_res(sle_answer, a, unit, input_user_true_or_false(
        'Хотите ли вы сделать копию исходных данных? (Введите "y" если да, "n" если нет) : '),
                  input_user_true_or_false(
                      'Хотите ли вы видеть действия программы над СЛАУ?(логи) (Введите "y" если да, "n" если нет) : '))
    inverse = inverse_matrix(res,b)  # Взять последние N строк, которые являются инвертированной матрицей
    final_matrix = try_res(sle_with_reverse_matrix, inverse, b)
    return print_answer(final_matrix)


def input_user_true_or_false(msg=''):
    answer = input(msg)
    if answer == 'y':
        return True
    if answer == 'n':
        return False
    if answer == '':
        return True
    else:
        show_info('Вы что-то неправильно ввели, попробуйте еще раз.')
        input_user_true_or_false()


def get_actions():
    return [sle_answer_action, reverse_action]
