import matplotlib.pyplot as plt


def show_array(res, msg):
    # Вывод массива на экран. View
    print(msg)
    print(res)


def show_error(err, msg):
    # Вывод ошибки на экран. View.
    print(msg)
    print(err)


def show_vectors(u, v):
    plt.title('Вектор')
    print(u)
    print(len(u))
    print(len(v))
    if len(u) > 1:
        u[1], v[0] = v[0], u[1]
    x = [0 for _ in u]
    y = x
    plt.quiver(x, y, u, v, angles='xy', scale_units='xy', scale=1, color='blue')
    plt.xlim(-max(u[0], v[0]) * 2.0, max(u[0], v[0]) * 2.0)
    plt.ylim(-max(u[0], v[0]) * 2.0, max(u[0], v[0]) * 2.0)
    plt.grid()
    plt.show()
