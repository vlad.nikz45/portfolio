def str_to_float_array(s):
    # Преобразование строки в массив. Converter.
    lst = s.split(',')
    a = [float(x) for x in lst]
    return a


def str_to_float_number(s):
    # Преобразование строки в число
    return float(s)
