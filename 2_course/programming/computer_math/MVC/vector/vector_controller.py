from vector.vector import add, sub, multiply_scalar, division_scalar, multiply_vectors, equality_vectors, \
    equality_accuracy_vectors, collinear_vectors, \
    oppositely_directed_vectors, orthogonal_vectors, length_vector, normirovla_vector, \
    direction_change_vector, angle_between_vectors_degrees, projection_vectors_scalar, projection_vectors_vector, \
    cos_between_vectors
from vector.vector_view import show_array, show_error, show_vectors
from vector.view_converter import str_to_float_array, str_to_float_number


def cos_between_vectors_action():
    a, b = input_two_arrays()
    safe_exec(cos_between_vectors, 'Косинус между векторами', a, b)


def collinear_vectors_action():
    a, b = input_two_arrays()
    safe_exec(collinear_vectors, 'Коллинеарность векторов', a, b)


def projection_vectors_vector_action():
    a, b = input_two_arrays()
    safe_exec(projection_vectors_vector, 'Векторная проекция вектора', a, b)


def projection_vectors_scalar_action():
    a, b = input_two_arrays()
    safe_exec(projection_vectors_scalar, 'Проекция вектора скалярная', a, b)


def angle_between_vectors_degrees_action():
    a, b = input_two_arrays()
    safe_exec(angle_between_vectors_degrees, 'Угол между векторами', a, b)


def direction_change_vector_action():
    a = input_one_array()
    safe_exec(direction_change_vector, 'Измененние направления вектора', a)


def normirovka_vector_action():
    a = input_one_array()
    safe_exec(normirovla_vector, ' Нормировка вектора', a)


def length_vector_action():
    a = input_one_array()
    safe_exec(length_vector, 'Длина вектора', a)


def orthogonal_vectors_action():
    a, b = input_two_arrays()
    safe_exec(orthogonal_vectors, 'Ортогональность векторов', a, b)


def oppositely_directed_vectors_action():
    a, b = input_two_arrays()
    safe_exec(oppositely_directed_vectors, 'Противоположные векторы', a, b)


def co_directed_vectors_action():
    a, b = input_two_arrays()
    safe_exec(collinear_vectors, 'Сонаправленность векторов', a, b)


def add_action():
    a, b = input_two_arrays()
    safe_exec(add, 'Сумма векторов', a, b)


def sub_action():
    a, b = input_two_arrays()
    safe_exec(sub, 'Вычитание векторов', a, b)


def multiply_scalar_action():
    a = input_one_array()
    b = input_scalar()
    safe_exec(multiply_scalar, 'Умножение на скаляр', a, b)


def division_scalar_action():
    a = input_one_array()
    b = input_scalar()
    safe_exec(division_scalar, 'Деление на скаляр', a, b)


def multiply_vectors_action():
    a, b = input_two_arrays()
    safe_exec(multiply_vectors, 'Умножение векторов', a, b)


def equality_vectors_action():
    a, b = input_two_arrays()
    safe_exec(equality_vectors, 'Равенство векторов', a, b)


def equality_accuracy_vectors_action():
    a, b = input_two_arrays()
    eps = input_one_number()
    safe_exec(equality_accuracy_vectors, 'Равенство векторов с заданной точностью', a, b, eps)


def safe_exec(func, funcname, *args):
    try:
        res = func(*args)
    except Exception as error:
        return show_error(error, 'Ошибка при вычислении')

    try:
        if len(res) > 2:
            raise ValueError('Можно отобразить только 2D векторы')
        return show_vectors([res[0]], [res[1]])
    except Exception as error:
        show_array(res, funcname)
        return show_error(error, 'Невозможно отобразить в matplotlib')


def input_two_arrays():
    try:
        s1 = input('Введите первый массив: ')
        a = str_to_float_array(s1)
        s2 = input('Введите второй массив: ')
        b = str_to_float_array(s2)
    except Exception as error:
        show_error(error, 'Ошибка при вводе, попробуйте еще раз')
        return input_two_arrays()
    return a, b


def input_one_array():
    try:
        s = input('Введите массив: ')
        a = str_to_float_array(s)
    except Exception as error:
        show_error(error, 'Ошибка при вводе, попробуйте еще раз')
        return input_one_array()
    return a


def input_scalar():
    try:
        s = input('Введите скаляр: ')
        a = abs(str_to_float_number(s))
    except Exception as error:
        show_error(error, 'Ошибка при вводе, попробуйте еще раз')
        return input_scalar()
    return a


def input_one_number():
    try:
        s = input('Введите число:  ')
        a = abs(str_to_float_number(s))
    except Exception as error:
        show_error(error, 'Ошибка при вводе числа, попробуйте еще раз')
        return input_one_number()
    return a


def get_actions():
    return [add_action, sub_action, multiply_scalar_action, division_scalar_action, multiply_vectors_action,
            equality_vectors_action,
            equality_accuracy_vectors_action, collinear_vectors_action, co_directed_vectors_action,
            oppositely_directed_vectors_action, orthogonal_vectors_action, length_vector_action,
            normirovka_vector_action, direction_change_vector_action, angle_between_vectors_degrees_action,
            projection_vectors_scalar_action, projection_vectors_vector_action, cos_between_vectors_action]
