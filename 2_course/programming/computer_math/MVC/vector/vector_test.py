import vector.vector as vec


def test_add():
    a = [1, 2, 5]
    b = [4, 8, 1]
    exp = [5, 10, 6]
    res = vec.add(a, b)
    assert res == exp


def test_sub():
    a = [1, 2, 5]
    b = [4, 8, 1]
    exp = [-3, -6, 4]
    res = vec.sub(a, b)
    assert res == exp


def test_multiply_scalar():
    a = [1, 2, 3]
    b = -5
    exp = [-5, -10, -15]
    res = vec.multiply_scalar(a, b)
    assert res == exp


def test_division_scalar():
    a = [1, 2, 3]
    b = 2
    exp = [0.5, 1, 1.5]
    res = vec.division_scalar(a, b)
    assert res == exp


def test_multiply_scalars():
    a = [1, 2, 3]
    b = [1, 2, 3]
    exp = 14
    res = vec.multiply_vectors(a, b)
    assert res == exp


def test_equality_vectors():
    a = [1, 2, 3]
    b = [1, 2, 3]
    exp = True
    res = vec.equality_vectors(a, b)
    assert res == exp


def test_equality_accuracy_vectors():
    a = [1, 2, 3]
    b = [0.6, 2, 3]
    eps = 0.5
    exp = True
    res = vec.equality_accuracy_vectors(a, b, eps)
    assert res == exp


def test_collinear_vectors():
    a = [1, 2, 3]
    b = [-1, -2, -3]
    exp = True
    res = vec.collinear_vectors(a, b)
    assert res == exp


def test_co_directed_vectors():
    a = [4, 0]
    b = [8, 0]
    exp = True
    res = vec.co_directed_vectors(a, b)
    assert res == exp


def test_oppositely_directed_vectors():
    a = [5, 0]
    b = [-5, 0]
    exp = True
    res = vec.oppositely_directed_vectors(a, b)
    assert res == exp


def test_orthogonal_vectors():
    a = [1, 2]
    b = [2, -1]
    exp = True
    res = vec.orthogonal_vectors(a, b)
    assert res == exp


def test_lenght_vector():
    a = [1, -3, 3, -1]
    exp = 2 * 5 ** 0.5
    res = vec.length_vector(a)
    assert res == exp


def test_normirovka_vector():
    a = [3, 5, 8]
    exp = [3 / 98 ** 0.5, 5 / 98 ** 0.5, 8 / 98 ** 0.5]
    res = vec.normirovla_vector(a)
    assert res == exp


def test_change_direction():
    a = [5, 10]
    exp = [-5, -10]
    res = vec.direction_change_vector(a)
    assert res == exp


def test_angle_degrees():
    a = [3, 4]
    b = [4, 3]
    exp = 16.26
    res = vec.angle_between_vectors_degrees(a, b)
    assert abs(res - exp) < 0.001


def test_projection_scalar():
    a = [1, 2]
    b = [3, 4]
    exp = 2.2
    res = vec.projection_vectors_scalar(a, b)
    assert res == exp


def test_projection_vectors_vector():
    a = [4, 5]
    b = [6, 0]
    exp = [4, 0]
    res = vec.projection_vectors_vector(a, b)
    assert res == exp


def test_cos_between_vectors():
    a = [3, 1]
    b = [2, 4]
    exp = 0.71
    res = round(vec.cos_between_vectors(a, b), 2)
    assert res == exp
