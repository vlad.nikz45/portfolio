import math

EPS = 1 / 2 ** 10

def check_equal_len(a, b):
    """Проверка равности исходных списков"""
    return len(a) == len(b)


def add(a, b):
    # Сложение векторов. Model
    return [x + y for x, y in zip(a, b)]


def sub(a, b):
    # Вычитание векторов. Model
    return [x - y for x, y in zip(a, b)]


def multiply_scalar(a, b):
    # Умножение вектора на скаляр
    return [i * b for i in a]


def division_scalar(a, b):
    # Деление вектора на скаляр
    return [i / b for i in a]


def multiply_vectors(a, b):
    # Умножение векторов
    return sum(x * y for x, y in zip(a, b))


def length_vector(a):
    # Длина вектора
    return sum([e * e for e in a]) ** 0.5


def cos_between_vectors(a, b):
    # Косинус между векторами
    return (multiply_vectors(a, b)) / (length_vector(a) * length_vector(b))


def collinear_vectors(a, b):
    # Коллинеарность векторов
    return abs(abs(cos_between_vectors(a, b)) - 1) < 1 / 2 ** 10


def co_directed_vectors(a, b):
    # Сонаправленые векторы
    return abs(cos_between_vectors(a, b) - 1) < EPS


def oppositely_directed_vectors(a, b):
    # Противоположно направленные векторы
    return abs(cos_between_vectors(a, b) - -1) < EPS


def equality_vectors(a, b):
    # Равенство векторов
    if (a == b) and (len(a) == len(b)):
        return True
    else:
        return False


def equality_accuracy_vectors(a, b, eps):
    # Равенство векторов с заданной точностью
    return False not in [abs(x - y) < eps for x, y in zip(a, b)] if len(a) == len(b) else False


def orthogonal_vectors(a, b):
    # Ортогональность векторов
    return abs(cos_between_vectors(a, b)) < EPS


def normirovla_vector(a):
    # Нормализация вектора
    return [i / length_vector(a) for i in a]


def direction_change_vector(a):
    # Изменение направления вектора на противоположный
    return [-i for i in a]


def angle_between_vectors_degrees(a, b):
    # Угол между векторами в градусах
    return math.acos(cos_between_vectors(a, b)) * 180 / math.pi


def projection_vectors_scalar(a, b):
    # Скалярная проекция вектора на вектор
    return multiply_vectors(a, b) / length_vector(b)


def projection_vectors_vector(a, b):
    # Векторная проекция вектора на вектор
    return multiply_scalar(normirovla_vector(b), projection_vectors_scalar(a, b))
