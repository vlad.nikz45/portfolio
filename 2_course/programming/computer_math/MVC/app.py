import approximation.approximation_controller as ap
import interpolation.interpolation_controller as interp
import maclaurin_series.maclaurin_controller as mc
import matrix.matrix_controller as mx
import sle.sle_controller as sle
import vector.vector_controller as vc


def select_action(functions):
    # Выбор действия из доступных
    is_run = True
    while is_run:
        print('\nДоступные операции: ')
        for i, item in enumerate(functions):
            print(i, item.__name__)
        try:
            i = int(input('\nВыберите операцию: '))
        except Exception as error:
            print(error, '\nОшибка при вводе, попробуйте еще раз')
            return select_action(functions)
        if 0 <= i < len(functions):
            function = functions[i]
            function()
        else:
            is_run = False


def select_controller(controllers):
    # Выбор контроллера из доступных
    is_run = True
    while is_run:
        print('\nДоступные контроллеры:')
        for i, item in enumerate(controllers):
            print(i, item.__name__)
        try:
            i = int(input('\nВыберите контроллер: '))
        except Exception as error:
            return print(error, '\nОшибка при вводе, попробуйте заново'), select_controller(controllers)
        if 0 <= i < len(controllers):
            controller = controllers[i]
            actions = controller.get_actions()
            select_action(actions)
        else:
            is_run = False


def get_controllers():
    # Получение доступных контроллеров
    return [vc, mx, sle, interp, ap, mc]


controllers = get_controllers()
select_controller(controllers)
