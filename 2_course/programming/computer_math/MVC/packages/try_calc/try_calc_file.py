from packages.try_calc.show_error import show_error


def try_calculate(func, *args):
    """Проверка на возможность вычисления данных. Условно Controller."""
    try:
        return func(*args)
    except ArithmeticError as err:
        return show_error(err, 'Ошибка при вычислении данных. Арифметическая ошибка')
    except IndexError as err:
        return show_error(err, 'Ошибка при вычислении данных. Выход за границы')
    except ValueError as err:
        return show_error(err, 'Ошибка при вычислении данных')
    except TypeError as err:
        return show_error(err, 'Ошибка при получении данных')
