def ensure(is_ok, msg=''):
    """Выкидывание ошибки"""
    if not is_ok:
        raise ValueError(msg)