import matplotlib.pyplot as plt


def show_2d_figure_result_matplotlib_axline(list_x_vectors, list_y_vectors, list_x_points, list_x_points2,
                                            list_y_points,
                                            list_y_points2, marker):
    """Вывод  графика"""
    points_x = [i for i in list_x_points]
    points_y = [i for i in list_y_points]
    points_x2 = [i for i in list_x_points2]
    points_y2 = [i for i in list_y_points2]
    vec_x = [i for i in list_x_vectors]

    vec_y = [i for i in list_y_vectors]
    plt.plot(vec_x, vec_y, marker=marker)
    plt.scatter(points_x, points_y, marker='o', color='orange')
    plt.scatter(points_x2, points_y2, marker='o')
    plt.axline((points_x[0][0], points_y[0][0]), (points_x[1][0], points_y[1][0]), linewidth=1)
    plt.title('График')
    plt.get_current_fig_manager().set_window_title('График')
    plt.grid()
    plt.show()


def show_2d_figure_result_matplotlib(list_x_vectors, list_y_vectors, list_x_points, list_y_points, marker):
    """Вывод  графика"""
    points_x = [i for i in list_x_points]
    points_y = [i for i in list_y_points]

    vec_x = [i for i in list_x_vectors]
    vec_y = [i for i in list_y_vectors]
    plt.plot(vec_x, vec_y, marker=marker)
    plt.scatter(points_x, points_y, marker='o', color='orange')
    plt.title('График')
    plt.get_current_fig_manager().set_window_title('График')
    plt.grid()
    plt.show()


def show_2d_figure_result_matplotlib_with_add_points(list_x_vectors, list_y_vectors, list_x_points, list_x_points2,
                                                     list_y_points,
                                                     list_y_points2, marker):
    """Вывод  графика"""
    points_x = [i for i in list_x_points]
    points_y = [i for i in list_y_points]
    points_x2 = [i for i in list_x_points2]
    points_y2 = [i for i in list_y_points2]
    vec_x = [i for i in list_x_vectors]

    vec_y = [i for i in list_y_vectors]
    plt.plot(vec_x, vec_y, marker=marker)

    plt.scatter(points_x2, points_y2, marker='o')
    plt.scatter(points_x, points_y, marker='o', color='orange')
    plt.title('График')
    plt.get_current_fig_manager().set_window_title('График')
    plt.grid()
    plt.show()


def show_two_2d_figure_result_matplotlib(list_x_vectors, list_y_vectors, add_list_x_vectors, add_list_y_vectors,
                                         marker):
    """Вывод  графика"""
    add_vec_x = [i for i in add_list_x_vectors]
    add_vec_y = [i for i in add_list_y_vectors]

    vec_x = [i for i in list_x_vectors]
    vec_y = [i for i in list_y_vectors]
    plt.ylim(-2, 5)
    plt.plot(vec_x, vec_y, marker=marker)
    plt.plot(add_vec_x, add_vec_y, marker=marker, color='orange')
    plt.title('График')
    plt.get_current_fig_manager().set_window_title('График')
    plt.grid()
    plt.show()
