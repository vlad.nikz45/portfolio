import matplotlib.pyplot as plt


def show_3d_figure_result_matplotlib(list_x_vectors, list_y_vectors, list_z_vectors, list_x_points, list_y_points,
                                     list_z_points):
    """Вывод 3D фигуры на экран в виде поверхности."""
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    vec_x = [i for i in list_x_vectors]
    vec_y = [i for i in list_y_vectors]
    vec_z = [i for i in list_z_vectors]

    surf = ax.plot_trisurf(vec_x, vec_y, vec_z, cmap='Blues', linewidth=0, antialiased=False, alpha=0.6)
    fig.colorbar(surf, shrink=0.5, aspect=5)

    points_x = [i for i in list_x_points]
    points_y = [i for i in list_y_points]
    points_z = [i for i in list_z_points]

    ax.scatter3D(points_x, points_y, points_z, color='RED', s=200)
    ax.set_title('Surface')
    plt.show()
