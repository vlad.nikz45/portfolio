from packages.view.show import show_error


def try_run_matplotlib(func, *args):
    try:
        func(*args)
    except Exception as err:
        return show_error(err, 'Ошибка при вызове библиотеки matplotlib')
