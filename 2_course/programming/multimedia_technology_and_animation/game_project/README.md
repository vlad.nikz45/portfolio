<h1 align="center">Game Project</h1>

### About task:
<p align="left">
Team project - Quest on the theme "New Year". 

The maximum number of people in the team is 3. 

The project must necessarily include:

Several scenes (minimum 3) 
Interactivity
Several types of animation
Sound and text effects
Camera control (not everywhere possible)
</p>

<h2></h2>

## Demonstration:

![1](images/1.png "1")
![2](images/2.png "2")
![3](images/3.png "3")
![4](images/4.png "4")
![5](images/5.png "5")
![6](images/6.png "6")
![7](images/7.png "7")
![8](images/8.png "8")
![9](images/9.png "9")
![10](images/10.png "10")
![11](images/11.png "11")
![12](images/12.png "12")
![13](images/13.png "13")
![14](images/14.png "14")
![15](images/15.png "15")
![16](images/16.png "16")
![17](images/17.png "17")
![18](images/18.png "18")
![19](images/19.png "19")
![20](images/20.png "20")
![21](images/21.png "21")
![22](images/22.png "22")
![23](images/23.png "23")
![24](images/24.png "24")
![25](images/25.png "25")
![26](images/26.png "26")
![27](images/27.png "27")
![28](images/28.png "28")
![29](images/29.png "29")
![30](images/30.png "30")
![31](images/31.png "31")
![32](images/32.png "32")
![33](images/33.png "33")
![34](images/34.png "34")
![35](images/35.png "35")
![36](images/36.png "36")
![37](images/37.png "37")
![38](images/38.png "38")
![39](images/39.png "39")
![40](images/40.png "40")
![41](images/41.png "41")
![42](images/42.png "42")
![43](images/43.png "43")
![44](images/44.png "44")
![45](images/45.png "45")
![46](images/46.png "46")
![47](images/47.png "47")
![48](images/48.png "48")
![49](images/49.png "49")
![50](images/50.png "50")
![51](images/51.png "51")
![52](images/52.png "52")
