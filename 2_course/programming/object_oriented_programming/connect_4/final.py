import time

import pygame


class Board:
    """Класс матрицы игрового поля"""

    def __init__(self):
        """Инициализация"""
        self.matrix = [[0 for _ in range(6)] for _ in range(6)]
        self.winning_sound = pygame.mixer.Sound('pobeda.mp3')
        self.draw_sound = pygame.mixer.Sound('draw.mp3')

    def winning_move(self, board, player_num):
        """Проверка выигранной комбинации"""
        if player_num % 2 == 0:
            player_num = 1
        else:
            player_num = 2
        # Проверка по горизонтали
        for c in range(len(self.matrix) - 3):
            for r in range(len(self.matrix)):
                if board[r][c] == player_num and board[r][c + 1] == player_num and board[r][c + 2] == player_num and \
                        board[r][
                            c + 3] == player_num:
                    return True

        # Проверка по вертикали
        for c in range(len(self.matrix)):
            for r in range(len(self.matrix) - 3):
                if board[r][c] == player_num and board[r + 1][c] == player_num and board[r + 2][c] == player_num and \
                        board[r + 3][c] == player_num:
                    return True

        # Справа налево диагональ
        for c in range(len(self.matrix) - 3):
            for r in range(len(self.matrix) - 3):
                if board[r][c] == player_num and board[r + 1][c + 1] == player_num and board[r + 2][
                    c + 2] == player_num and board[r + 3][c + 3] == player_num:
                    return True

        # Слева направо диагональ
        for c in range(len(self.matrix) - 3):
            for r in range(3, len(self.matrix)):
                if board[r][c] == player_num and board[r - 1][c + 1] == player_num and board[r - 2][
                    c + 2] == player_num and \
                        board[r - 3][c + 3] == player_num:
                    return True

    @staticmethod
    def transposition_matrix(m1):
        """Транспозиция матрицы"""
        return [list(i) for i in zip(*m1)]

    def check_full_column(self, column):
        """Проверка, заполнен ли столбец"""
        copy_matrix = self.transposition_matrix(self.matrix)
        for i in range(len(copy_matrix)):
            if i == column:
                if copy_matrix[i][0] != 0:
                    return True
        return False

    def check_full_board(self):
        """Проверка, заполнено ли поле(для ничьи)"""
        count__fulled_cells = 0
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                if self.matrix[i][j] != 0:
                    count__fulled_cells += 1
        if count__fulled_cells == 36:
            return True
        return False

    def click_on_column(self, column, turn):
        """Обработка клика по столбцу, добавление фишки игрока в ячейку игрового поля"""
        copy_matrix = self.transposition_matrix(self.matrix)
        for index_column in range(len(self.matrix)):
            if index_column == column:
                for elem in range(len(copy_matrix[index_column])):
                    if copy_matrix[index_column][elem] != 0:
                        if turn % 2 == 0:
                            copy_matrix[index_column][elem - 1] = 1
                        else:
                            copy_matrix[index_column][elem - 1] = 2
                        break
                    if copy_matrix[index_column][-1] == 0:
                        if turn % 2 == 0:
                            copy_matrix[index_column][- 1] = 1
                        else:
                            copy_matrix[index_column][- 1] = 2
                        break
        copy_matrix = self.transposition_matrix(copy_matrix)
        return copy_matrix


class Drawing(Board):
    """Класс, отрисовывающий матрицу игровго поля"""

    def __init__(self):
        """Инициализация"""
        super().__init__()
        self.__circle_radius = 100

    @staticmethod
    def create_circle(win, x, y, color, circle_radius):
        """Отрисовка фишки"""
        pygame.display.update(
            pygame.draw.circle(win, 'black', (x + circle_radius / 2, y + circle_radius / 2), circle_radius / 2 - 1,3))
        pygame.display.update(
            pygame.draw.circle(win, color, (x + circle_radius / 2, y + circle_radius / 2), circle_radius / 2 - 3))

    def draw_mousemotion(self, win, turn, column):
        """Динамическая отрисовка фишки игрока, чей сейчас ход, снизу"""
        matrix = self.click_on_column(column, turn)
        self.visualize_grid(matrix, win)

    def visualize_grid(self, matrix, win):
        """Отрисовка игрового поля"""
        y = 100
        for row in matrix:
            x = 0
            for item in row:
                if item == 0:
                    self.create_circle(win, x, y, 'white', self.__circle_radius)
                elif item == 2:
                    self.create_circle(win, x, y, 'red', self.__circle_radius)
                else:
                    self.create_circle(win, x, y, 'yellow', self.__circle_radius)
                x += self.__circle_radius
            y += self.__circle_radius
        pygame.display.update()

    @staticmethod
    def visualize_winning_text(win, player_num):
        """Отрисовка победного текста"""
        f1 = pygame.font.Font(None, 56)
        text1 = f1.render(f'Победил {player_num}!', True,
                          'green')
        win.blit(text1, (135, 30))
        pygame.display.update()

    @staticmethod
    def visualize_draw_text(win):
        """Отрисовка текста ничьи"""
        f1 = pygame.font.Font(None, 56)
        text1 = f1.render('Ничья!', True,
                          'green')
        win.blit(text1, (250, 30))
        pygame.display.update()


class Game:
    """Класс, хранящий данные об игровом окне Pygame"""

    def __init__(self):
        self.__screen_width = 600
        self.__screen_height = 600
        self.__columns = 6
        self.win = pygame.display.set_mode((600, 700))
        self.win.fill((30, 144, 255))
        self.rect_list = [[pygame.rect.Rect(self.__screen_width / self.__columns * i, 0,
                                            self.__screen_width / self.__columns,
                                            self.__screen_height)] for i in range(self.__columns)]
        self.fishka_sound = pygame.mixer.Sound('fishka.mp3')


def main():
    """Игровой цикл"""
    pygame.init()
    game = Game()
    drawing = Drawing()
    drawing.visualize_grid(drawing.matrix, game.win)
    pygame.display.flip()
    running = True
    turn = 0
    pygame.mixer.music.load('fon.mp3')
    pygame.mixer.music.set_volume(0.3)
    pygame.mixer.music.play(-1)
    while running:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEMOTION:
                for i in range(len(game.rect_list)):
                    if game.rect_list[i][0].collidepoint(event.pos):
                        if not drawing.check_full_column(i):
                            drawing.draw_mousemotion(game.win, turn, i)
            pygame.display.update()
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONUP:
                for i in range(len(game.rect_list)):
                    if game.rect_list[i][0].collidepoint(event.pos):
                        if not drawing.check_full_column(i):
                            drawing.matrix = drawing.click_on_column(i, turn)
                            drawing.visualize_grid(drawing.matrix, game.win)
                            game.fishka_sound.play()
                            if not drawing.check_full_board():

                                if drawing.winning_move(drawing.matrix, turn):
                                    if turn % 2 == 0:
                                        drawing.visualize_winning_text(game.win, 'желтый')
                                    else:
                                        drawing.visualize_winning_text(game.win, 'красный')
                                    pygame.mixer.music.stop()
                                    drawing.winning_sound.play()
                                    time.sleep(3)
                                    exit(main())
                                turn += 1
                                drawing.draw_mousemotion(game.win, turn, i)
                            else:
                                pygame.mixer.music.stop()
                                drawing.visualize_draw_text(game.win)
                                drawing.draw_sound.play()
                                time.sleep(3)
                                exit(main())


main()
