class MinesweeperController:
    """Контроллер сапёра"""

    def set_model(self, model):
        """Установка модели"""
        self.__model = model

    def set_view(self, view):
        """Установка представления"""
        self.__view = view

    def set_player(self, player):
        """Установка игрока"""
        self.__player = player

    def start_new_game(self):
        """Запуск новой игры"""

        # -----------------------
        # НЕДОДЕЛАННАЯ ФУНКЦИЯ  |
        # -----------------------

        self.__player.init_or_restore_states_after_game_over()
        self.__view.is_game_continue = True
        self.__view.coins_data_list = []
        self.__view.hearts_data_list = []
        self.__view.level_was_not_entered = True
        self.__view.sound_result_not_played = False

        # game_settings = self.__view.get_game_settings()
        # try:
        #     self.__model.__init__(*map(int, game_settings))
        # except Exception:
        #     self.__model.__init__(self.__model.row_number, self.__model.column_number, self.__model.mine_number,
        #                           self.__model.mine_damage, self.__model.hearts_drop_number,
        #                           self.__model.coins_drop_number)
        self.__view.show_menu_lvl()
        # self.__view.create_board()

    def on_left_click(self, row, column):
        """Обработка левого клика"""
        damage = self.__model.open_cell(row, column)
        if damage != 0:
            self.__player.health_points -= damage
        print('current hp is', self.__player.health_points)
        self.check_lose()
            # self.start_new_game()

        self.check_win()
            # self.start_new_game()

        # elif self.__model.is_game_over():
        #     self.__model.open_all_bombs()
        #     self.__view.show_game_over_message()
        # self.start_new_game()

    def check_lose(self):
        """Проверка на поражение"""
        if self.__player.health_points <= 0:
            self.__model.open_all_bombs()
            self.__view.show_game_over_message()

    def check_win(self):
        """Проверка на победу"""
        if self.__model.is_win():
            self.__model.do_flag_board()
            self.__view.show_win_message()
            self.__player.lvl_is_not_complete_list[self.__view.index_level] = False
            self.__player.save_states_current_player()

    def on_left_click_on_restart(self):
        """Обработка левого клика на кнопку старта заново"""
        self.start_new_game()

    def current_hp_player(self):
        """Возврат нынешнего здоровья игрока"""
        return self.__player.health_points

    def max_hp_player(self):
        return self.__player.max_health_points

    def current_lvl_is_not_complete_list(self):
        """Возврат нынешнего списка не пройденных уровней"""
        return self.__player.lvl_is_not_complete_list

    def current_coins_player(self):
        """Возврат нынешнего количества монет игрока"""
        return self.__player.number_coins

    def current_inventory_player(self):
        """Возврат нынешного инвентаря игрока"""
        return self.__player.inventory

    def current_shop_items(self):
        """Возврат нынешнего состояния предметов в магазине"""
        return self.__player.shop_items

    def current_cost_items_shop(self):
        """Возврат нынешнего состояния цен на предметы в магазине"""
        return self.__player.cost_items_shop

    def on_left_click_opened(self, number, row, column, is_have_heart, is_have_coin, mixer, is_have_inventory_item,
                             id_inventory_item):
        """Обработка левого клика на открытую ячейку"""
        if is_have_heart:
            if self.__player.health_points >= self.__player.max_health_points:
                print('i cant take anymore')
                mixer.play_put_flag_sound(0.3)
            else:
                self.__player.health_points += 1
                self.__model.remove_heart_at_cell(row, column)
                print('i feel better now')
                mixer.play_pickup_sound(0.3)
        elif is_have_coin:
            mixer.play_pickup_sound(0.3)
            print("i get coin")
            self.__player.number_coins += 1
            self.__model.remove_coin_at_cell(row, column)
        elif is_have_inventory_item:
            if 0 in self.__player.inventory:
                mixer.play_pickup_sound(0.3)
                print("i get item")
                self.__player.inventory[self.__player.inventory.index(0)] = id_inventory_item
                self.__player.save_states_current_player()
                self.__model.remove_item_at_cell(row, column)
            else:
                mixer.play_put_flag_sound(0.3)
                print('i cant take anymore, inventory is full')

        if number == self.__model.count_flags_around_cell(row, column) + \
                self.__model.count_opened_and_mined_around_cell(row, column):
            sum_damage = self.__model.open_neighbours(row, column)
            if sum_damage != 0:
                self.__player.health_points -= sum_damage
        self.on_left_click(row, column)

    def on_right_click(self, row, column):
        """Обработка правого клика"""
        self.__model.next_cell_mark(row, column)

    def on_right_click_opened(self, number, row, column):
        """Обработка правого клика на открытую ячейку"""
        if number == self.__model.count_close_around_cell(row, column) + \
                self.__model.count_opened_and_mined_around_cell(row, column):
            self.__model.do_flag_neighbours(row, column)
        self.on_right_click(row, column)

    def on_left_click_on_cell_inventory(self, cell, mixer):
        """Нажатие на ячейку инвентаря"""
        if self.__player.inventory[cell] != 0:
            print(self.__player.inventory[cell])
            if self.__player.inventory[cell] != 5:
                self.__player.max_health_points += self.__player.inventory[cell]
                self.__player.health_points += self.__player.inventory[cell]
            else:
                self.__model.open_cell_by_cell_opening_staff()
                self.check_win()
            self.__player.inventory[cell] = 0
            self.__player.save_states_current_player()
            mixer.play_pickup_sound(0.3)
        else:
            mixer.play_put_flag_sound(0.3)

    def on_left_click_on_cell_shop(self, cell, mixer):
        """Нажатие на ячейку в магазине предметов"""
        if self.__player.shop_items[cell] != 0:
            print(self.__player.shop_items[cell])
            if 0 in self.__player.inventory:
                if self.__player.number_coins >= self.__player.cost_items_shop[cell]:
                    self.__player.number_coins -= self.__player.cost_items_shop[cell]
                    mixer.play_pickup_sound(0.3)
                    print("i bought item")
                    self.__player.inventory[self.__player.inventory.index(0)] = self.__player.shop_items[cell]
                    self.__player.shop_items[cell] = 0
                    self.__player.save_states_current_player()
                else:
                    mixer.play_put_flag_sound(0.3)
                    print('i dont have enough money')
            else:
                mixer.play_put_flag_sound(0.3)
                print('i cant take anymore, inventory is full')
        else:
            mixer.play_put_flag_sound(0.3)

    def open_main_menu(self):
        """Открытие меню"""
        self.__view.show_main_menu()
