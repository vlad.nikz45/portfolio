from time import time as timer
import pygame


class Explosion(pygame.sprite.Sprite):
    """Событие, по которому происходит взрыв. На вход должен подаваться список спрайтов"""

    def __init__(self, images: list, x: float, y: float, explosion_speed: int = 2):  # x,y центр спрайтов
        """Инициализация"""
        pygame.sprite.Sprite.__init__(self)
        self.__images = images
        self.__index = 0
        # доступ к конкретному спрайту по индексу
        self.__image = self.__images[self.__index]
        # rect из img
        self.__rect = self.__image.get_rect()
        # центр img
        self.__rect.center = [x, y]
        self.__counter = 0
        self.__explosion_speed = explosion_speed
        self.__explosion_is_not_updating = True

    def update(self):
        """Обновление анимации. Контроллер"""
        long_update_in_sec = 0.03
        if self.__explosion_is_not_updating:
            self.__explosion_is_not_updating = False
            self.__update()
        elif timer() - self.__start_last_update_time >= long_update_in_sec:
            self.__explosion_is_not_updating = True
            self.__update()

    def __update(self):
        """Обновление анимации"""
        self.__start_last_update_time = timer()
        # скорость обновления анимации взрыва
        self.__counter += 1
        # переход на следующий спрайт и проверка за выход из списка изображений
        if self.__counter >= self.__explosion_speed and self.__index < len(self.__images) - 1:
            self.__counter = 0
            # следующий img
            self.__index += 1
            self.__image = self.__images[self.__index]
        # если анимация завершена, то экземпляр удаляется
        if self.__index >= len(self.__images) - 1:
            self.kill()

    @property
    def image(self):
        """Возврат текущей картинки"""
        return self.__image

    @property
    def rect(self):
        """Возврат текущего коллайдера"""
        return self.__rect


def main():
    """Запуск модуля"""
    
    def draw_bg():
        """Отрисовка заливки"""
        screen.fill(bg)

    def draw_cell(size: int = 75):
        """Отрисовка ячейки"""
        cell_surf = pygame.Surface((size, size))
        cell_surf.fill(cell_colour)
        cell_rect = pygame.Rect(screen_width // 2, screen_height // 2, size, size)
        screen.blit(cell_surf, cell_rect)
        return cell_rect

    def load_sprite_explosion(size: int = 100):
        """Загрузка и хранение спрайтов в списке, для настройки размера задать число в size"""
        images = []
        # Загрузка спрайтов по индексу n с помощью цикла
        for n in range(0, 12 + 1):
            img = pygame.image.load(f'../images/explosion_sprites/explosion_{n}.png')
            # изменение размера через size
            img = pygame.transform.scale(img, (size, size))
            images.append(img)
        # получение готового списка спрайтов
        return images

    pygame.init()

    clock = pygame.time.Clock()
    fps = 60

    screen_width = 600
    screen_height = 600

    screen = pygame.display.set_mode((screen_width, screen_height))
    pygame.display.set_caption('взрыв')

    bg = (50, 50, 50)
    cell_colour = (252, 186, 3)

    explosion_group = pygame.sprite.Group()
    boom = load_sprite_explosion(175)

    run = True
    while run:
        clock.tick(fps)
        draw_bg()
        cell = draw_cell()
        explosion_group.draw(screen)
        explosion_group.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                if cell.collidepoint(event.pos):
                    explosion = Explosion(boom, cell.centerx, cell.centery)
                    explosion_group.add(explosion)

        pygame.display.update()

    pygame.quit()


if __name__ == "__main__":
    main()
