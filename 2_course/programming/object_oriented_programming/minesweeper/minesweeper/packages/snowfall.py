import pygame
import random
import sys


class Snow:
    """класс снежинки"""

    def __init__(self, screen, x, y, MAX_X, MAX_Y, snowflakes_image_list, speed_x: float = 4,
                 speed_y_division: float = 1):
        """инициализация"""
        self.__x = x
        self.__y = y
        self.__MAX_X = MAX_X
        self.__MAX_Y = MAX_Y
        self.__screen = screen
        self.__snowflakes_image_list = snowflakes_image_list
        self.__speed_y_division = speed_y_division
        self.__speed = random.randrange(1, 3)
        self.__speed_x = speed_x
        self.__index = random.randint(1, len(self.__snowflakes_image_list))

    def move_snow(self):
        """перемещение снежинки"""
        self.__y += self.__speed / self.__speed_y_division
        if self.__y > self.__MAX_Y:
            self.__y = 0
        # Move RIGHT
        self.__x += self.__speed_x
        if self.__x > self.__MAX_X:
            self.__x = 0

    def draw_snow(self):
        """отрисовка по координатам"""
        self.__screen.blit(self.__snowflakes_image_list[self.__index - 1], (self.__x, self.__y))


class Snowfall:
    """класс снегопада"""

    def __init__(self, screen, MAX_X, MAX_Y, snowflakes_image_list, max_snowflakes, speed_x: float = 4,
                 speed_y_division: float = 1):
        """инициализация"""
        self.__screen = screen
        self.__MAX_X = MAX_X
        self.__MAX_Y = MAX_Y
        self.__snowfall = []
        self.__snowflakes_image_list = snowflakes_image_list
        self.__max_snowflakes = max_snowflakes
        self.__speed_x = speed_x
        self.__speed_y_division = speed_y_division
        self.initialize_snow()

    def initialize_snow(self):
        """create list with obj class Snow()"""
        for i in range(0, self.__max_snowflakes):
            xx = random.randint(0, self.__MAX_X)
            yy = random.randint(0, self.__MAX_Y)
            self.__snowfall.append(Snow(self.__screen, xx, yy, self.__MAX_X, self.__MAX_Y,
                                        self.__snowflakes_image_list, self.__speed_x, self.__speed_y_division))

    def start_snowfall(self):
        """для каждой снежинки выполняется метод отрисовки и передвежения"""
        for i in self.__snowfall:
            i.move_snow()
            i.draw_snow()


def main():
    """Запуск модуля"""
    pygame.init()
    screen = pygame.display.set_mode((600, 600))

    def check_for_exit():
        """функция выхода из программы"""
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

    def rand_images():
        """создание списка снежинок с разными спрайтами и размером"""
        snowlakes_image_list = []
        for j in range(3, 10):
            for i in range(1, 4):
                image_filename = "../images/snowflakes/snowflake_" + str(i) + ".png"
                image = pygame.image.load(image_filename).convert_alpha()
            image = pygame.transform.scale(image, (j, j))
            snowlakes_image_list.append(image)

        return snowlakes_image_list

    snowflakes_image_list = rand_images()
    snow_fall = Snowfall(screen, 600, 600, snowflakes_image_list, 10)

    while True:
        screen.fill("black")
        snow_fall.start_snowfall()

        check_for_exit()
        pygame.time.Clock().tick(60)
        pygame.display.flip()


if __name__ == "__main__":
    main()
