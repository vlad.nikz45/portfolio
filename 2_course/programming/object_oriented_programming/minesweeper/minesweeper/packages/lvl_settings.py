import json


class LvlSettings:
    def __init__(self, headline, row_num: int, column_num: int, bomb_num: int, bomb_damage: tuple, hearts_drop_num: int,
                 coins_drop_num: int):
        self.__headline = headline
        self.__row_num = row_num
        self.__column_num = column_num
        self.__bomb_num = bomb_num
        self.__bomb_damage = bomb_damage
        self.__hearts_drop_num = hearts_drop_num
        self.__coins_drop_num = coins_drop_num

    @property
    def get_coins_drop_num(self):
        return self.__coins_drop_num

    @property
    def get_row_num(self):
        return self.__row_num

    @property
    def get_column_num(self):
        return self.__column_num

    @property
    def get_bomb_num(self):
        return self.__bomb_num

    @property
    def get_bomb_damage(self):
        return self.__bomb_damage

    @property
    def get_hearts_drop_num(self):
        return self.__hearts_drop_num

    @property
    def get_headline(self):
        return self.__headline

    @property
    def get_bomb_damage_for_text(self):
        symbols = ['(', ')', ',']
        text = ''
        for i in self.__bomb_damage:
            if i in symbols:
                continue
            else:
                text = text + i
        return text[0] + '-' + text[1]


class LoadLvlSettings:
    def __init__(self, lvl_num):
        self.__lvl_num = lvl_num

    def parsing_lvl(self):
        settings = []
        with open(f'lvl_settings/{self.__lvl_num}.json', encoding='utf-8') as json_file:
            data = json.load(json_file)
            for d in data['settings']:
                settings.append(d["headline"])
                settings.append(d["row_number"])
                settings.append(d["column_number"])
                settings.append(d["bomb_number"])
                settings.append(d["bomb_damage"])
                settings.append(d["hearts_drop_number"])
                settings.append(d["coins_drop_number"])
            return settings

# # Пример работы
# level1_settings = LvlSettings(*LoadLvlSettings(1).parsing_lvl())
# print(level1_settings.get_coins_drop_num)
# print(level1_settings.get_bomb_damage_for_text)
