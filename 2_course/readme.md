## Папка: 2 курс

### Обзор
Эта папка содержит коллекцию моих работ, выполненных в течение второго курса университетских занятий. Проекты разделены на две основные секции: **Дизайн** и  **Программирование**.

### Содержание
1. [Дизайн](#дизайн)
   1.1 [Создание визуальной айдентики для эко-магазина](design/ecohata_brief)
   1.2 [Брендбук ПИ](design/PI_brandbook)
   1.3 [Проект дизайна сайта для шаурмичной](design/project_site_design)
  
2. [Программирование](#программирование)
   2.1 [Компьютерная математика](programming/computer_math)
   2.2 [Хакатон 2 курс](programming/cave_of_time)
   2.3 [Мультимедиа технологии и анимация](programming/multimedia_technology_and_animation)
   2.4 [Проект по английскому за второй курс](programming/request_game)
  
3. [Объектно-ориентированное программирование](#object_oriented_programming)
   3.1 [Connect 4](programming/object_oriented_programming/connect_4)
   3.2 [Сапёр](programming/object_oriented_programming/minesweeper)
  
4. [Веб-разработка](#web)
   4.1 [Форма регистрации на конференцию](programming/web/conference_registration_form)
   4.2 [Сапёр](programming/web/minesweeper)
   4.3 [Головоломка](programming/web/pazzle)
   4.4 [Регулярные выражения PHP](programming/web/regular_expressions_php)
   4.5 [Список дел](programming/web/todo_list)

## 1. Дизайн<a name="дизайн"></a>

#### 1.1 Экологический бриф<a name="ecohata_brief"></a>
   - Описание: Проект по созданию визуальной айдентики для эко-магазина.
   - [Посмотреть Экологический бриф](design/ecohata_brief)

#### 1.2 Брендбук ПИ<a name="PI_brandbook"></a>
   - Описание: Работа с брендбуком ПИ.
   - [Посмотреть Брендбук ПИ](design/PI_brandbook)

#### 1.3 Дизайн сайта проекта<a name="project_site_design"></a>
   - Описание: Проект веб-дизайна для сайта шаурмичной.
   - [Посмотреть Дизайн сайта ](design/project_site_design)

## 2. Программирование<a name="программирование"></a>

#### 2.1 Компьютерная математика<a name="computer_math"></a>
   - Описание: Калькулятор на MVC модели.
   - [Изучить калькулятор](programming/computer_math)

#### 2.2 Хакатон 2 курс<a name="cave_of_time"></a>
   - Описание: Игра на Renpy, хоррор-квест.
   - [Изучить пещеру](programming/cave_of_time)

#### 2.3 Мультимедиа технологии и анимация<a name="multimedia_technology_and_animation"></a>
   - Описание: Новогодний проект, сделанный на Adobe Animate.
   - [Сыграть](programming/multimedia_technology_and_animation)

#### 2.4 Проект по английскому за 2 курс<a name="request_game"></a>
   - Описание: Проект на английском языке, генератор историй с использованием DALLE и CHATGPT
   - [Посмотреть](programming/request_game)

## 3. Объектно-ориентированное программирование<a name="object_oriented_programming"></a>

#### 3.1 Connect 4<a name="connect_4"></a>
   - Описание: Игра "Connect 4" с использованием объектно-ориентированного подхода.
   - [Играть в Connect 4](programming/object_oriented_programming/connect_4)

#### 3.2 Сапёр<a name="minesweeper"></a>
   - Описание: Реализация игры "Сапёр" с использованием объектно-ориентированного программирования.
   - [Играть в Сапёр](programming/object_oriented_programming/minesweeper)

## 4. Веб-разработка<a name="web"></a>

#### 4.1 Форма регистрации на конференцию<a name="conference_registration_form"></a>
   - Описание: Разработка формы регистрации для конференции.
   - [Изучить Форму регистрации на конференцию](programming/web/conference_registration_form)

#### 4.2 Сапёр<a name="minesweeper_web"></a>
   - Описание: Веб-версия игры "Сапёр".
   - [Играть в Сапёр](programming/web/minesweeper)

#### 4.3 Головоломка<a name="pazzle"></a>
   - Описание: Веб-головоломка.
   - [Решить Головоломку](programming/web/pazzle)

#### 4.4 Регулярные выражения на PHP<a name="reg_ul"></a>
   - Описание: Регулярные выражения на PHP различных видов.
   - [Посмотреть регулярные выражения](programming/web/regular_expressions_php)

#### 4.5 Список дел<a name="todo_list"></a>
   - Описание: Веб-приложение для управления списком дел.
   - [Использовать Список дел](programming/web/todo_list)

### Навигация
- Для изучения конкретного проекта, просто щелкните по гиперссылке, предоставленной рядом с описанием каждого проекта.
