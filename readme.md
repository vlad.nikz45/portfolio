<h1 align="center">Электронное портфолио</h1>

# Резюме:

ОБРАЗОВАНИЕ:
- студент ИГУ ФБКИ направления Прикладная Информатика в дизайне, 3 курс


ПРОФЕССИОНАЛЬНЫЕ НАВЫКИ:
- Владение иностранным языком (английский язык),
- умение работать в команде,
- адаптивность,
- уверенное владение русским языком,
- коммуникабельность,
- стремление к профессиональному росту,
- ответственное отношение к работе.

ЗНАНИЕ КОМПЬЮТЕРНЫХ ПРОГРАММ:
- Adobe Photoshop,
- CoreI Draw,
- Animate,
- Figma,
- GIT,
- Microsoft Office,
- DaVinci Resolve,
- TouchDesigner.

ЗНАНИЕ ЯЗЫКОВ ПРОГРАМИРОВАНИЯ:
- Свободное владение Python, 
- базовые знания синтаксиса и стандартных операций:
    - JavaScript, 
    - HTML&CSS, 
    - QML, 
    - PHP, 
    - SQL, 
    - Kotlin (Android Studio).


### В данном репозитории я попытался собрать самые значимые работы за время обучения в университете.

# Работы разбил по курсам:
<h1 align="center"><a href="https://gitlab.com/vlad.nikz45/portfolio/-/tree/main/1_course?ref_type=heads">Работы за первый курс</a></h1>

<h1 align="center"><a href="https://gitlab.com/vlad.nikz45/portfolio/-/tree/main/2_course?ref_type=heads">Работы за второй курс</a></h1>

<h1 align="center"><a href="https://gitlab.com/vlad.nikz45/portfolio/-/tree/main/3_course?ref_type=heads">Работы за третий курс</a></h1>