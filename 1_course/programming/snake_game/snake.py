import pygame
import random
import pygame_menu
pygame.init()
display_width = 400
display_height = 400
display = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('Змейка')
clock = pygame.time.Clock()
snake_block = 10

def our_snake(snake_block, snake_list):
    for x in snake_list:
        pygame.draw.rect(display, 'yellow', [x[0], x[1], snake_block, snake_block])
def enemy(snake_block, enemy_snake_list):
    for i in enemy_snake_list:
        pygame.draw.rect(display, 'red', [i[0], i[1],snake_block, snake_block])

def message_box(score):
    menu_end = pygame_menu.Menu('Вы проиграли!', display_width, display_height, theme=pygame_menu.themes.THEME_ORANGE)
    menu_end.add.label(name[0])
    menu_end.add.label('Вы набрали ' + str(score) + ' очков')
    menu_end.add.button('Играть снова', gameLoop)
    menu_end.add.button('Выйти', pygame_menu.events.EXIT)
    menu_end.mainloop(display)

def gameLoop():
    global name
    global direction
    direction=''
    game_end = False
    lose = False
    score = 0
    x1 = display_width // 2
    y1 = display_height // 2
    enemy_x = display_width
    enemy_y = display_height
    flag = -1
    x1_change = 0
    y1_change = 0
    snake_coord = []
    snake_length = 1
    enemy_snake_coord = []
    enemy_snake_length = 1
    text = menu.get_input_data()
    name = list(text.values())
    applex = round(random.randrange(0, display_width - snake_block) / 10.0) * 10.0
    appley = round(random.randrange(0, display_height - snake_block) / 10.0) * 10.0

    pygame.mixer.pre_init(44100, 16, 2, 256)
    pygame.mixer.Sound('hurt.mp3')
    pygame.mixer.music.set_volume(0.2)
    pygame.mixer.music.load("fon.mp3")
    pygame.mixer.music.set_volume(0.1)
    pygame.mixer.music.play(-1)
    while not game_end:
        if lose == True:
            message_box(score)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_end = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and flag != 0:
                    x1_change = -snake_block
                    y1_change = 0
                    flag = 1
                elif event.key == pygame.K_RIGHT and flag != 1:
                    x1_change = snake_block
                    y1_change = 0
                    flag = 0
                elif event.key == pygame.K_UP and flag != 2:
                    x1_change = 0
                    y1_change = -snake_block

                    flag = 3
                elif event.key == pygame.K_DOWN and flag != 3:
                    y1_change = snake_block
                    x1_change = 0
                    flag = 2

        if x1 >= display_width or x1 < 0 or y1 >= display_height or y1 < 0:
            lose = True
            pygame.mixer.Sound('hurt.mp3').play()
        x1 += x1_change
        y1 += y1_change
        display.fill('purple')
        pygame.draw.rect(display, 'green', [applex, appley, snake_block, snake_block])
        block_add = []
        enemy_add = []
        enemy_add.append(enemy_x)
        enemy_add.append(enemy_y)
        block_add.append(x1)
        block_add.append(y1)
        snake_coord.append(block_add)
        enemy_snake_coord.append(enemy_add)
        if len(snake_coord) > snake_length:
            del snake_coord[0]
        if len(enemy_snake_coord) > enemy_snake_length:
            del enemy_snake_coord[0]
        for j in snake_coord[:-1] :
            if j == block_add:
                lose = True
                pygame.mixer.Sound('hurt.mp3').play()
            elif j == enemy_add:
                enemy_snake_length = 1
                enemy_snake_coord.clear()
                enemy_x = display_width
                enemy_y = display_height

        for n in enemy_snake_coord[:-1]:
            if n == block_add:
                lose = True
                pygame.mixer.Sound('hurt.mp3').play()
        our_snake(snake_block, snake_coord)
        enemy(snake_block,enemy_snake_coord)
        if x1 == applex and y1 == appley:
            applex = round(random.randrange(0, display_width - snake_block) / 10.0) * 10.0
            appley = round(random.randrange(0, display_height - snake_block) / 10.0) * 10.0
            snake_length += 1
            score += 1
        if enemy_x == applex and enemy_y == appley:
            applex = round(random.randrange(0, display_width - snake_block) / 10.0) * 10.0
            appley = round(random.randrange(0, display_height - snake_block) / 10.0) * 10.0
            enemy_snake_length += 1
        if enemy_x > applex and ([enemy_x - 10, enemy_y] not in enemy_snake_coord) and (direction!='right'):
            enemy_x-=10
            direction = 'left'
        elif enemy_x < applex and ([enemy_x + 10, enemy_y] not in enemy_snake_coord) and (direction!='left'):
            enemy_x+=10
            direction = 'right'
        elif enemy_y > appley and ([enemy_x, enemy_y - 10] not in enemy_snake_coord) and (direction!='down'):
            enemy_y-=10
            direction = 'up'
        elif enemy_y < appley and ([enemy_x,enemy_y + 10] not in enemy_snake_coord) and (direction!='up'):
            enemy_y+=10
            direction='down'
        else:
            enemy_snake_length = 1
            enemy_snake_coord.clear()
            enemy_x = display_width
            enemy_y = display_height
        pygame.display.update()
        clock.tick(15)

doc = pygame_menu.Menu('Змейка', display_width, display_height, theme=pygame_menu.themes.THEME_ORANGE)
doc.add.label(' Движение ').set_margin(0, 30)
doc.add.label(' Вниз : down arrow ')
doc.add.label(' Вверх : up arrow ')
doc.add.label(' Влево : left arrow ')
doc.add.label(' Вправо : right arrow ')
menu = pygame_menu.Menu('Змейка', display_width, display_height, theme=pygame_menu.themes.THEME_ORANGE)
menu.add.text_input('Ваше имя : ', default='Иван', maxchar=8).set_margin(0, 10).get_value()
menu.add.button('Играть', gameLoop).set_padding(10)
menu.add.button('Управление', doc).set_padding(10)
menu.add.button('Выйти', pygame_menu.events.EXIT).set_padding(10)
menu.mainloop(display)










